# CMake generated Testfile for 
# Source directory: /home/yigit/catkin_ws/src
# Build directory: /home/yigit/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(vrep_common)
SUBDIRS(control_unit)
SUBDIRS(cover_control)
SUBDIRS(keyboard_control)
SUBDIRS(ros_bubble_rob)
SUBDIRS(sensor_control)
SUBDIRS(straight)
SUBDIRS(turn)
SUBDIRS(vrep_joy)
SUBDIRS(vrep_plugin)
SUBDIRS(vrep_skeleton_msg_and_srv)
SUBDIRS(vrep_plugin_skeleton)
SUBDIRS(wall_follower)
