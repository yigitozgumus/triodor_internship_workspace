//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <math.h>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"



// Used API services:
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosReadDistance.h"
#include "vrep_common/simRosEnableSubscriber.h"
#include "vrep_common/simRosSetJointTargetPosition.h"


bool simulationRunning = true;
float simulationTime=0.0f;

float motorValue = 0.0 ;

void infoCallback(const vrep_common::VrepInfo::ConstPtr& info){
	simulationTime=info->simulationTime.data;
	simulationRunning = (info->simulatorState.data&1)!=0;
}

void motorCallBack(const std_msgs::Float64::ConstPtr& info){
	motorValue = info->data ;
}

int main(int argc,char* argv[]){
	int motorHandle = 0.0;

	if(argc >= 2){
		motorHandle = atoi(argv[1]);
	}else {
		std::cout << "Indicate following arguments: leftMotorHandle rightMotorHandle sensorHandle!"<< std::endl;
		sleep(5000);
		return 0 ;
	}

	//Creation of the Ros Node
	int _argc = 0 ;
	char** _argv = NULL ;
	std::string nodeName("coverControl");

	// ROS node initialization
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);
	
	ros::NodeHandle nh("coverControl");
	//Subscription to V-REP's info stream
	//SUBSCRIBER FOR V-REP
	ros::Subscriber defaultSub=nh.subscribe("/vrep/info",1,infoCallback);

	// ros::ServiceClient client_enableSubscriber=nh.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
	// 	vrep_common::simRosEnableSubscriber srv_enableSubscriber;
	// 	srv_enableSubscriber.request.topicName="/"+nodeName+"/motor"; // the topic name
	// 	srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
	// 	srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_position; // the subscriber type
	// 	srv_enableSubscriber.request.auxInt1 = motorHandle;
	// 	srv_enableSubscriber.request.auxInt2 = -1 ;
	// 	srv_enableSubscriber.request.auxString = "";

	ros::ServiceClient client_enableSubscriber=nh.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
		vrep_common::simRosEnableSubscriber srv_enableSubscriber;
		srv_enableSubscriber.request.topicName="/"+nodeName+"/motor"; // the topic name
		srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
		srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_state; // the subscriber type
		srv_enableSubscriber.request.auxInt1 = -1;
		srv_enableSubscriber.request.auxInt2 = -1 ;
		srv_enableSubscriber.request.auxString = "";

	if ( client_enableSubscriber.call(srv_enableSubscriber)&&(srv_enableSubscriber.response.subscriberID!=-1)){

		ros::Subscriber motorTake = nh.subscribe("/coverControl/motorValue",1,motorCallBack);
	//	ros::Publisher motorPub = nh.advertise<std_msgs::Float64>("/coverControl/motor",1);
		ros::Publisher motorPubState = nh.advertise<vrep_common::JointSetStateData>("motor",1);
		vrep_common::JointSetStateData motorSpeeds;

	while (ros::ok()&&simulationRunning)
		{ // this is the control loop
			//this is a comment
			 std::cout << "test" << std::endl ;
			// std_msgs::Float64 msg ;
			// msg.data = motorValue ;
			// motorPub.publish(msg);
			motorSpeeds.handles.data.push_back(motorHandle);
			motorSpeeds.setModes.data.push_back(1);
			motorSpeeds.values.data.push_back(motorValue);
			motorSpeeds.handles.data.push_back(motorHandle);
			motorSpeeds.setModes.data.push_back(2);
			motorSpeeds.values.data.push_back(1);
			motorPubState.publish(motorSpeeds);

			// handle ROS messages:
				ros::spinOnce();

				// sleep a bit:
				usleep(1000);
		}

	}
	return 0 ;
}
