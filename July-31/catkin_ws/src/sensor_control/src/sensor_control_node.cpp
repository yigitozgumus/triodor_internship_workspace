//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <math.h>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"



// Used API services:
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosReadDistance.h"
#include "vrep_common/simRosEnableSubscriber.h"

#define PI 3.14159265
//Global Variables and Publisher subscriber Callbacks

bool simulationRunning = true;
float simulationTime=0.0f;

float leftSpeedValue = 0.0 ;
float rightSpeedValue = 0.0 ;


float coverTurnValue = 160 ;
float skirtTurnValue = 0.0;

bool isCoverSet = false;

float x[2],y[2],z[2] = {} ; // Sensor data message

float detectedDistance[2] = {};


ros::Timer timeCheck;

void infoCallback(const vrep_common::VrepInfo::ConstPtr& info){
	simulationTime=info->simulationTime.data;
	simulationRunning = (info->simulatorState.data&1)!=0;
}
void timerCallBack(const ros::TimerEvent& event){
	leftSpeedValue = 0.0 ;
	rightSpeedValue = 0.0 ;
	std::cout << "make it zero\n";
	timeCheck.stop();
}

void leftVelocityCallBack(const std_msgs::Float32::ConstPtr& info){

	if(timeCheck.isValid()){
		timeCheck.stop();
		timeCheck.start();
	}

	leftSpeedValue = info->data;
	//std::cout << "first = " << initialZ << " current = " << zRot << std::endl ;
}

void rightVelocityCallBack(const std_msgs::Float32::ConstPtr& info){

	if(timeCheck.isValid()){
		timeCheck.stop();
		timeCheck.start();
	}

	rightSpeedValue = info->data;
	//std::cout << "first = " << initialZ << " current = " << zRot << std::endl ;
}

void wallInfoCallBack(const std_msgs::Float32::ConstPtr& info){
	// if(!info->data){ // if zero for left, if one for right
	//coverTurnValue = 1.0 ;

	// }else {
	//coverTurnValue = -1.0 ;
	// }
	coverTurnValue = info->data ;
	std::cout << coverTurnValue << std::endl;
	if(!isCoverSet)
	isCoverSet = true ;
	//std::cout << "Calibrated" << std::endl ;
}
void leftDistanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){

	x[0] = info->detectedPoint.x ;
	y[0] = info->detectedPoint.y ;
	z[0] = info->detectedPoint.z ;

	detectedDistance[0] = std::sqrt(std::pow(x[0],2) + std::pow(y[0],2) + std::pow(z[0],2));

	//detectedDistance[0] = sensorToWall[0];
	//std::cout <<"Left detected =  "<< detectedDistance[0]  << std::endl ;

}

void rightDistanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){

	x[1] = info->detectedPoint.x ;
	y[1] = info->detectedPoint.y ;
	z[1] = info->detectedPoint.z ;

	detectedDistance[1] = std::sqrt(std::pow(x[1],2) + std::pow(y[1],2) + std::pow(z[1],2));

	//detectedDistance[1] = sensorToWall[1];
	//std::cout <<" Right detected =  "<< detectedDistance[1] << std::endl;
}

void skirtInfoCallBack(const std_msgs::Float32::ConstPtr& info){

	skirtTurnValue = info->data ;
}


int main(int argc,char* argv[]){
// sensor handle and motor handles
	int leftMotorHandle ;
	int rightMotorHandle;
	int leftSensorHandle;
	int rightSensorHandle ;
	int bodyHandle ;
	int coverHandle;
	int skirtHandle ;

	if(argc >= 8){

		leftMotorHandle = atoi(argv[1]);
		rightMotorHandle = atoi(argv[2]);
		leftSensorHandle = atoi(argv[3]);
		rightSensorHandle = atoi(argv[4]);
		bodyHandle = atoi(argv[5]);
		coverHandle = atoi(argv[6]);
		skirtHandle = atoi(argv[7]);

	} else {
		std::cout << "Indicate following arguments: leftMotorHandle rightMotorHandle sensorHandle!"<< std::endl;
		sleep(5000);
		return 0 ;
	}

	//Creation of the Ros Node
	int _argc = 0 ;
	char** _argv = NULL ;
	std::string nodeName("sensorControl");

	// ROS node initialization
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	//Create a node handle
	ros::NodeHandle nh("sensorControl");
	timeCheck = nh.createTimer(ros::Duration(1), timerCallBack,false);
	//Subscription to V-REP's info stream
	//SUBSCRIBER FOR V-REP
	ros::Subscriber defaultSub=nh.subscribe("/vrep/info",1,infoCallback);

	//Create a subscriber for the geometry_msgs/twist
	//SUBSCRIBER FOR GEOMETRY_MSGS/TWIST
	ros::Subscriber leftVelocityInfo = nh.subscribe("/sensorControl/leftVelocity",1,leftVelocityCallBack);

	ros::Subscriber rightVelocityInfo = nh.subscribe("/sensorControl/rightVelocity",1,rightVelocityCallBack);


	//SUBSCRIBER FOR COVER AND SKIRT MOTOR TURN VALUES

	ros::Subscriber DirectionInfo = nh.subscribe("/sensorControl/coverDirection",1,wallInfoCallBack);

	ros::Subscriber SkirtDirectionInfo = nh.subscribe("/skirtDirection",1,skirtInfoCallBack);

	//Create a publisher for the location of the robot
	//PUBLISHER FOR LOCATION OR ROBOT
	ros::ServiceClient clien_locationPublisher=nh.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
	vrep_common::simRosEnablePublisher srv_locationPub ;
	srv_locationPub.request.topicName="RobotLocation";
	srv_locationPub.request.queueSize=1;
	srv_locationPub.request.streamCmd=simros_strmcmd_get_object_pose ;
	srv_locationPub.request.auxInt1=bodyHandle;
	srv_locationPub.request.auxInt2 = -1 ;

	//Create a publisher for the proximity sensor
	//PUBLISHER FOR SENSOR DATA
	ros::ServiceClient client_enablePublisherL=nh.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
	vrep_common::simRosEnablePublisher srv_enablePublisherL;
	srv_enablePublisherL.request.topicName="LeftProximitySensorData"; // the requested topic name
	srv_enablePublisherL.request.queueSize=1; // the requested publisher queue size (on V-REP side)
	srv_enablePublisherL.request.streamCmd=simros_strmcmd_read_proximity_sensor; // the requested publisher type
	srv_enablePublisherL.request.auxInt1=leftSensorHandle; // some additional information the publisher needs (what proximity sensor)

	ros::ServiceClient client_enablePublisherR=nh.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
	vrep_common::simRosEnablePublisher srv_enablePublisherR;
	srv_enablePublisherR.request.topicName="RightProximitySensorData"; // the requested topic name
	srv_enablePublisherR.request.queueSize=1; // the requested publisher queue size (on V-REP side)
	srv_enablePublisherR.request.streamCmd=simros_strmcmd_read_proximity_sensor; // the requested publisher type
	srv_enablePublisherR.request.auxInt1=rightSensorHandle; // some additional information the publisher needs (what proximity sensor)

	//This publisher sends the relative distance to the detected object to wallFollower node

	if(clien_locationPublisher.call(srv_locationPub) && srv_locationPub.response.effectiveTopicName.length()!=0){

		if ( (client_enablePublisherL.call(srv_enablePublisherL)&&(srv_enablePublisherL.response.effectiveTopicName.length()!=0) ) &&
		(client_enablePublisherR.call(srv_enablePublisherR)&&(srv_enablePublisherR.response.effectiveTopicName.length()!=0) ) ){


		ros::Subscriber subLeft =nh.subscribe("/vrep/LeftProximitySensorData",1,leftDistanceCallBack);

		ros::Subscriber subRight =nh.subscribe("/vrep/RightProximitySensorData",1,rightDistanceCallBack);


		//SUBSCRIBER FOR MOTOR SPEED, (FOR V-REP)
		ros::ServiceClient client_enableSubscriber=nh.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
		vrep_common::simRosEnableSubscriber srv_enableSubscriber;
		srv_enableSubscriber.request.topicName="/"+nodeName+"/wheels"; // the topic name
		srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
		srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_state; // the subscriber type
		srv_enableSubscriber.request.auxInt1 = -1;
		srv_enableSubscriber.request.auxInt2 = -1 ;
		srv_enableSubscriber.request.auxString = "";
		//SUBSCRIBER FOR THE COVER TURN (FOR V-REP)

		if ( client_enableSubscriber.call(srv_enableSubscriber)&&(srv_enableSubscriber.response.subscriberID!=-1)){


			//&& client_enableSubscriberC.call(srv_enableSubscriberC)&&(srv_enableSubscriberC.response.subscriberID!=-1)
			// ok, the service call was ok, and the subscriber was succesfully started on V-REP side
			// V-REP is now listening to the desired motor joint states

			//publisher for the V-REP motor speed subscriber
			//PUBLISHER FOR JOINT_SET_DATA (MOTORSPEED)
			ros::Publisher motorSpeedPub=nh.advertise<vrep_common::JointSetStateData>("wheels",1);
			//ros::Publisher completeMsg= nh.advertise<std_msgs::Bool>("Permission",1);
			//ros::Publisher coverAnglePub=nh.advertise<std_msgs::Float64>("coverAngle",1);

			ros::Publisher leftDistancePub = nh.advertise<std_msgs::Float32>("leftDistance",1);

			ros::Publisher rightDistancePub = nh.advertise<std_msgs::Float32>("rightDistance",1);

			ros::Publisher hardwareOk = nh.advertise<std_msgs::Bool>("/sensorControl/hardwareOk",1);

			vrep_common::JointSetStateData motorSpeeds;
			vrep_common::JointSetStateData coverMsg ;

			while (ros::ok()&&simulationRunning)
			{ // this is the control loop

				std_msgs::Bool hardwareCheck ;
				hardwareCheck.data = true ;
				hardwareOk.publish(hardwareCheck);

				//Proximity sensor message publishers
				std_msgs::Float32 leftMessage ;
				std_msgs::Float32 rightMessage ;

				std::cout << coverTurnValue << std:: endl ;

				leftMessage.data = detectedDistance[0];
				rightMessage.data = detectedDistance[1];
				leftDistancePub.publish(leftMessage);
				rightDistancePub.publish(rightMessage);


					// coverMsg.handles.data.push_back(coverHandle);
					// coverMsg.setModes.data.push_back(0);
					// coverMsg.values.data.push_back(coverTurnValue);

					motorSpeeds.handles.data.push_back(leftMotorHandle);
					motorSpeeds.handles.data.push_back(rightMotorHandle);
					motorSpeeds.handles.data.push_back(coverHandle);
					motorSpeeds.handles.data.push_back(skirtHandle);

					motorSpeeds.setModes.data.push_back(2);
					motorSpeeds.setModes.data.push_back(2); // 2 is the speed mode
					motorSpeeds.setModes.data.push_back(2);
					motorSpeeds.setModes.data.push_back(2);


					motorSpeeds.values.data.push_back(leftSpeedValue);
					motorSpeeds.values.data.push_back(rightSpeedValue);
					motorSpeeds.values.data.push_back(coverTurnValue);
					motorSpeeds.values.data.push_back(skirtTurnValue);

					motorSpeedPub.publish(motorSpeeds);

			//	}
				// handle ROS messages:
				ros::spinOnce();

				// sleep a bit:
				usleep(1000);
			}
		}

	}

	}


	ros::shutdown();
	printf("SensorControlNode just ended!\n");

	return 0 ;
}
