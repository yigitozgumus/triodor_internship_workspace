set(_CATKIN_CURRENT_PACKAGE "wall_follower")
set(wall_follower_MAINTAINER "yigit <yigitozgumus1@gmail.com>")
set(wall_follower_DEPRECATED "")
set(wall_follower_VERSION "0.0.0")
set(wall_follower_BUILD_DEPENDS "geometry_msgs" "sensor_msgs" "opencv2" "vrep_common" "roscpp")
set(wall_follower_RUN_DEPENDS "geometry_msgs" "roscpp")
set(wall_follower_BUILDTOOL_DEPENDS "catkin")