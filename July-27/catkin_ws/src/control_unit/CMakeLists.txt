cmake_minimum_required(VERSION 2.8.3)
project(control_unit)

find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS std_msgs image_transport vrep_common)

include_directories(${catkin_INCLUDE_DIRS})
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

add_executable(controlUnit src/ros_control_node.cpp) 
target_link_libraries(controlUnit ${catkin_LIBRARIES})
add_dependencies(controlUnit vrep_common_generate_messages_cpp)
