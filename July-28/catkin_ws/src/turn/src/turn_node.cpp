//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>

#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"

#define PI 3.14159265
#define WHEEL_DISTANCE 0.741

bool isHardwareOk = false ;
bool turnCompletion = false;

float angle = 0.0 ;
int direction = 0.0 ;
float radius = 0.0 ;


float locations[3] = {};
float rotations[3] = {};
float q0,q1,q2,q3 = 0.0;
//First location values of the robot 
float locationF[3] = {} ;
// check variable for initial rotation
bool initialRotations = false ; 
bool initialLocations = false ;
//initial alpha rotation value
float initialRot = 0.0;

bool simulationGO = false; 
bool ordersTaken = false ;

float afterCross = 0.0 ;

void hardwareCallBack(const std_msgs::Bool::ConstPtr& check){
	isHardwareOk = check->data;
}

void locCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] = 180 + atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		initialRotations = true ;
		turnCompletion = false ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;
		initialLocations = true; 
	}

	
}

void motionInfoCallBack(const std_msgs::Float32MultiArray::ConstPtr& info){
	//angle-direction-radius
	if(turnCompletion){
		ordersTaken = false ;
		initialRotations = false ;
	}
	if(!ordersTaken){
		angle = info->data[0];
		direction = info->data[1];
		radius = info->data[2];
		ordersTaken = true ;
		
		simulationGO = true ;
	}
}

std_msgs::Float32MultiArray startTurn(){
	std_msgs::Float32MultiArray msg; 
	
	float leftSpeed = 0.0 ;
	float rightSpeed = 0.0 ;
	if(angle > 360)
		angle -=360 ; ;
	//very Complex soon to be garbage or magic
	afterCross = (1 - (direction * 2)) * angle + initialRot ;
	if(afterCross < 0){
		if(std::fabs(rotations[0]-initialRot) < initialRot){
			leftSpeed = (radius?0.7:0.5) * (radius?(radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2))):((direction * 2) -1));
			rightSpeed = (radius?0.7:0.5) * (radius?(radius + ((1 - (direction * 2)) * (WHEEL_DISTANCE/2))):(1 - (direction * 2)));
		}else if((360 - rotations[0])  < std::fabs(afterCross)){
			leftSpeed = (radius?0.7:0.5) * (radius?(radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2))):((direction * 2) -1));
			rightSpeed = (radius?0.7:0.5) * (radius?(radius + ((1 - (direction * 2)) * (WHEEL_DISTANCE/2))):(1 - (direction * 2)));
		}else{
			leftSpeed = 0.0 ;
			rightSpeed = 0.0 ;
			turnCompletion = true;
		}
	}else if(afterCross > 360) {
		if(rotations[0] < 360 && rotations[0] >= initialRot){
			leftSpeed = (radius?0.7:0.5) * (radius?(radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2))):((direction * 2) -1));
			rightSpeed = (radius?0.7:0.5) * (radius?(radius + ((1 - (direction * 2)) * (WHEEL_DISTANCE/2))):(1 - (direction * 2)));
		}else if(rotations[0] < (angle + initialRot - 360)){
			leftSpeed = (radius?0.7:0.5) * (radius?(radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2))):((direction * 2) -1));
			rightSpeed = (radius?0.7:0.5) * (radius?(radius + ((1 - (direction * 2)) * (WHEEL_DISTANCE/2))):(1 - (direction * 2)));
		}else{
			leftSpeed = 0.0 ;
			rightSpeed = 0.0 ;
			turnCompletion = true;
		}
	}else{
		if(std::fabs(rotations[0] - initialRot) < angle){
			leftSpeed = (radius?0.7:0.5) * (radius?(radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2))):((direction * 2) -1));
			rightSpeed = (radius?0.7:0.5) * (radius?(radius + ((1 - (direction * 2)) * (WHEEL_DISTANCE/2))):(1 - (direction * 2)));
		}else{
			leftSpeed = 0.0 ;
			rightSpeed = 0.0 ;
			turnCompletion = true;
		}
	}

	// if(radius == 0){
	// 	//0 is left 1 is right
	// 	if(std::fabs(rotations[0] - initialRot) < angle){
	// 		leftSpeed = 0.6 * ((direction * 2) -1) ;
	// 		rightSpeed = 0.6 *(1 - (direction * 2));
	// 	//	(radius:(radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2)))?((direction * 2) -1))
	// 	//	leftSpeed = (radius?0.7:0.5) * (radius?(radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2))):((direction * 2) -1));
	// 	//	rightSpeed = (radius?0.7:0.5) * (radius?(radius + ((1 - (direction * 2)) * (WHEEL_DISTANCE/2))):(1 - (direction * 2)));
	// 	}
	// }else{
	// 	if(std::fabs(rotations[0] - initialRot) < angle){
	// 		leftSpeed = 0.8 * (radius + ( ((direction * 2) -1) *(WHEEL_DISTANCE/2))) ;
	// 		rightSpeed = 0.8 * (radius + ((1 - (direction * 2)) * (WHEEL_DISTANCE/2)));
	// 	}
		
	// }

	// if(std::fabs(rotations[0] - initialRot) > angle){
	// 	leftSpeed = 0.0 ;
	// 	rightSpeed = 0.0 ;
	// 	turnCompletion = true;
	// }

	msg.data.push_back(leftSpeed);
	msg.data.push_back(rightSpeed);
	return msg ;

}



int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("turnMotion");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("turnMotion");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS------------------------------------------------------

	ros::Subscriber hardwareInfo = node.subscribe("/sensorControl/hardwareOk",1,hardwareCallBack);

	ros::Subscriber localSub = node.subscribe("/vrep/RobotLocation",1,locCallBack);

	ros::Subscriber turnSub = node.subscribe("/turnDirective",1,motionInfoCallBack);


	//ALL PUBLISHERS ------------------------------------------------------

	ros::Publisher leftVelocityP = node.advertise<std_msgs::Float32>("/sensorControl/leftVelocity",1);

	ros::Publisher rightVelocityP = node.advertise<std_msgs::Float32>("/sensorControl/rightVelocity",1);

	ros::Publisher completionInfo = node.advertise<std_msgs::String>("CompletedInfo",1);


	//Simulation main loop
	while(ros::ok){
		if(isHardwareOk){

			if(ordersTaken){
				simulationGO = !turnCompletion ;
			}
			std::cout << rotations[0] << std::endl;
			if(simulationGO){
				std_msgs::Float32 left ;
				std_msgs::Float32 right ;
				std_msgs::Float32MultiArray msg ;

				msg = startTurn();

				left.data = msg.data[0];
				right.data = msg.data[1];

				leftVelocityP.publish(left);
				rightVelocityP.publish(right);
				std::cout << "turn motion in progress" << std::endl ;
				std::cout << rotations[0] - initialRot << std::endl ;

			}if(turnCompletion){
				std_msgs::String msg ;
				msg.data = "I completed the task!\n";
				completionInfo.publish(msg);
				std::cout << "turn motion completed "<< std::endl ;
			}
		}
		// handle ROS messages:
		ros::spinOnce();

		// sleep a bit:
		usleep(1000);

	}
	
	return 0 ;
}