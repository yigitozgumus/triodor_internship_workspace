//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
//#include "vrep_common/JointSetStateData.h"
#include "vrep_common/simRosEnablePublisher.h"

#define THROTTLE_SPEED 1.6f 
#define PROPORTIONAL_CONSTANT 0.55f
#define DERIVATIVE_CONSTANT 0.08f
#define INTEGRAL_CONSTANT 0.000005f

#define PI 3.14159265

// if it is zero, left sensor data, if it is one, right sensor data
#define DIRECTION 1
#define FOLLOW_DISTANCE 2.0f
#define TRAVEL_DISTANCE 10.0f


//Global variables

int detectedObj = 0 ;
int detectedObject = 0 ;
float relativeDistance = 0;
bool simulationRunning = true ;
float initialRot = 0.0;
bool initialPosition = false ;
float previousError = 0.0f;
float totalError = 0.0f ;


float initialX, initialY , initialZ = 0.0 ;
float xRot, yRot,zRot  = 0.0 ;
float q0,q1,q2,q3 = 0.0;
float x[2],y[2],z[2] = {} ; // Sensor data message
//Twist message arrays

float detectedDistance[2] = {};
float sensorToWall[2]={};
int objectDistanceHandle = 0;

bool simulationGOGO = false ;

//Callback functions
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info)
{
	simulationRunning=(info->simulatorState.data&1)!=0;
}

void sensorCallBack(const std_msgs::Float32::ConstPtr& msg){
	relativeDistance = msg->data ;
	std::cout << "Relative distance =  " << relativeDistance << std::endl;


}

void leftDistanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){
	objectDistanceHandle = info->detectedObject.data ;
	x[0] = info->detectedPoint.x ;
	y[0] = info->detectedPoint.y ;
	z[0] = info->detectedPoint.z ;

	sensorToWall[0] = std::sqrt(std::pow(x[0],2) + std::pow(y[0],2) + std::pow(z[0],2));
	detectedDistance[0] = (sensorToWall[0] * cos((initialRot - xRot) * PI / 180.0));
	std::cout <<"Left detected =  "<< detectedDistance[0]  << std::endl ;

}

void rightDistanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){
	objectDistanceHandle = info->detectedObject.data ;
	x[1] = info->detectedPoint.x ;
	y[1] = info->detectedPoint.y ;
	z[1] = info->detectedPoint.z ;

	sensorToWall[1] = std::sqrt(std::pow(x[1],2) + std::pow(y[1],2) + std::pow(z[1],2));
	detectedDistance[1] = (sensorToWall[1] * cos((initialRot - xRot) * PI / 180.0));
	std::cout <<" Right detected =  "<< detectedDistance[1] << std::endl;
}
void locationCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	xRot = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	yRot = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	zRot = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << xRot - initialRot  << std::endl ;
	if(!initialPosition){
		initialRot = xRot;
		initialPosition = true ;
	}
	
}
void permissionCallBack(const std_msgs::Bool::ConstPtr& gogo){
	if(gogo->data){
		simulationGOGO = true ;
	}
}



std_msgs::Float32MultiArray manoeuvre(float currentDistance[DIRECTION]){
	//Create a default message 
	std_msgs::Float32MultiArray msg ;
	//error for the current distance to the obstacle
	float errorHandle = currentDistance[DIRECTION] - FOLLOW_DISTANCE ;
	totalError += errorHandle ;
	float errorC_P = errorHandle - previousError ;

	float manouevreValue = (PROPORTIONAL_CONSTANT * errorHandle) +
							 (DERIVATIVE_CONSTANT * errorC_P) + 
							 (INTEGRAL_CONSTANT * totalError) ;
	previousError = errorHandle ;

	float leftSpeed = 0;
	float rightSpeed =0;
	if(currentDistance[DIRECTION] > FOLLOW_DISTANCE){
		leftSpeed = THROTTLE_SPEED + (manouevreValue * (DIRECTION));
		rightSpeed = THROTTLE_SPEED + (manouevreValue * (1 - DIRECTION) ) ;
		
	}else{	
		leftSpeed =THROTTLE_SPEED - (manouevreValue * (1 - DIRECTION));
		rightSpeed=THROTTLE_SPEED - (manouevreValue * (DIRECTION )) ;
		
	}
	if(  (xRot - initialRot) < -15 && (leftSpeed > rightSpeed) ){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;
		
	}else if( (xRot - initialRot) > 15 && ( rightSpeed > leftSpeed )){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;	
			
		}

			msg.data.push_back(leftSpeed);
			msg.data.push_back(rightSpeed);
			return msg ;
		

	
}

int main(int argc,char* argv[]){

	int _argc = 0;
	char** _argv = NULL;
	struct timeval tv;
	unsigned int timeVal=0;
	if (gettimeofday(&tv,NULL)==0)
		timeVal=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
	std::string nodeName("wallFollower");
	// std::string randId(boost::lexical_cast<std::string>(timeVal+int(999999.0f*(rand()/(float)RAND_MAX))));
	// nodeName+=randId;
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("WallNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	ros::Subscriber subInfo=node.subscribe("/vrep/info",1,infoCallback);

	ros::Subscriber subLeft =node.subscribe("/vrep/LeftProximitySensorData",1,leftDistanceCallBack);;
	ros::Subscriber subRight =node.subscribe("/vrep/RightProximitySensorData",1,rightDistanceCallBack);;
	
	 ros::Subscriber locSub = node.subscribe("/vrep/RobotLocation",1,locationCallBack);
	 ros::Subscriber permissionSub = node.subscribe("/sensorControl/Permission",1,permissionCallBack); 

	ros::Publisher VelocityPub = node.advertise<std_msgs::Float32MultiArray>("velocity",1);
	ros::Publisher DirectionInfo = node.advertise<std_msgs::Int32>("whichWall",1);

	while(ros::ok && simulationRunning){

		std_msgs::Int32 initial ;
		initial.data = DIRECTION ;
		DirectionInfo.publish(initial);

		if(simulationGOGO){
		std_msgs::Float32MultiArray msg ;
		msg = manoeuvre(detectedDistance);
		VelocityPub.publish(msg);
	}

		ros::spinOnce() ;

		usleep(1000);
	}

	return 0 ;
}
