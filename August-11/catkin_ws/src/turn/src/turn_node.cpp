//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>

#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"

#include "vrep_common/JointSetStateData.h"
#include "vrep_common/VrepInfo.h"


#define PI 3.14159265
#define WHEEL_DISTANCE 0.741

bool isHardwareOk = false ;
bool turnCompletion = false;
float leftID,rightID,coverID,skirtID = 0.0 ;

bool simulationRunning = true;
float simulationTime=0.0f;

float angle = 0.0 ;
int direction = 0.0 ;
float radius = 0.0 ;

float pauseMotion = false ;

float locations[3] = {};
float rotations[3] = {};
float q0,q1,q2,q3 = 0.0;
//First location values of the robot 
float locationF[3] = {} ;
// check variable for initial rotation
bool initialRotations = true ; 
bool initialLocations = false ;
//initial alpha rotation value
float initialRot = 0.0;

bool simulationGO = false; 
bool ordersTaken = false ;
bool completeMessage = true ;

float afterCross = 0.0 ;

void hardwareCallBack(const std_msgs::Bool::ConstPtr& check){
	isHardwareOk = check->data;
}

void locCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] =  atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		turnCompletion = false ;
        simulationGO = true ;
        completeMessage = true ;
        initialRotations = true ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;
		initialLocations = true; 
	}

	
}

void motionInfoCallBack(const std_msgs::Float32MultiArray::ConstPtr& info){

	 	initialRotations = false ;
		angle = info->data[0];
		direction = info->data[1];
		radius = info->data[2];
		ordersTaken = true ;
}

vrep_common::JointSetStateData startTurn(){
	vrep_common::JointSetStateData msg; 
	
	float leftSpeed = 0.0 ;
	float rightSpeed = 0.0 ;
	if(angle > 360)
		angle -=360 ; 
	afterCross = initialRot + angle ;
	if(afterCross < -180){
		afterCross = afterCross + 360 ;
	}else if(afterCross > 180){
		afterCross = afterCross - 360 ;
	}

	if(rotations[0] < afterCross - 2.0 || rotations[0] > afterCross + 2.0){
		leftSpeed = (radius?(0.8 * direction):0.5) *
		(radius?(radius + ( (angle>0?(-1 * direction):(1 * direction)) *(WHEEL_DISTANCE/2))):(angle>0?-1:1));
		rightSpeed = (radius?(0.8 * direction):0.5) *
		(radius?(radius + ((angle>0?(1 * direction):(-1 * direction)) * (WHEEL_DISTANCE/2))):(angle>0?1:-1));
	}else if (rotations[0] > afterCross - 2.0 && rotations[0] < afterCross + 2.0){
			leftSpeed = 0.0;
            rightSpeed =0.0;
            turnCompletion = true ;
		}
			
	msg.handles.data.push_back(leftID);
    msg.setModes.data.push_back(2);
    msg.values.data.push_back(leftSpeed);

    msg.handles.data.push_back(rightID);
    msg.setModes.data.push_back(2);
    msg.values.data.push_back(rightSpeed);
	return msg ;

}
void pauseCallBack(const std_msgs::Empty::ConstPtr& info){
	pauseMotion = true ;
}
void resumeCallBack(const std_msgs::Empty::ConstPtr& info){
	pauseMotion = false ;
}
void terminateCallBack(const std_msgs::Empty::ConstPtr& info){
	ordersTaken = false ;
	simulationGO = false ;
}

void leftMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& left){
    leftID = left->data ;
}
void rightMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& right){
    rightID = right->data ;
}
void coverMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& cover){
    coverID = cover->data ;
}
void skirtMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& skirt){
    skirtID = skirt->data ;

}

void infoCallback(const boost::shared_ptr<vrep_common::VrepInfo>& info){
    simulationTime=info->simulationTime.data;
    simulationRunning = (info->simulatorState.data&1)!=0;
}

int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("turnMotion");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("turnMotion");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS------------------------------------------------------

	ros::Subscriber hardwareInfo = node.subscribe("/sensorControl/hardwareOk",1,hardwareCallBack);

	ros::Subscriber localSub = node.subscribe("/vrep/RobotLocation",1,locCallBack);

	ros::Subscriber turnSub = node.subscribe("/turnDirectives",1,motionInfoCallBack);

	ros::Subscriber pauseInfo = node.subscribe("/controls/pause",1,pauseCallBack);

	ros::Subscriber resumeInfo = node.subscribe("/controls/resume",1,resumeCallBack);

	ros::Subscriber terminateInfo = node.subscribe("/controls/terminate",1,terminateCallBack);

    ros::Subscriber leftMotorIDInfo = node.subscribe("/motorID/left",1,leftMotorIDCallBack);

    ros::Subscriber rightMotorIDInfo = node.subscribe("/motorID/right",1,rightMotorIDCallBack);

    ros::Subscriber coverMotorIDInfo = node.subscribe("/motorID/cover",1,coverMotorIDCallBack);

    ros::Subscriber skirtMotorIDInfo = node.subscribe("/motorID/skirt",1,skirtMotorIDCallBack);

	//ALL PUBLISHERS ------------------------------------------------------

	
	ros::Publisher completionInfo = node.advertise<std_msgs::String>("CompletedInfo",1);

    ros::Publisher motorSpeedPub=node.advertise<vrep_common::JointSetStateData>("/Motors",1);

    vrep_common::JointSetStateData motorSpeeds ;
	//Simulation main loop
	while(ros::ok){
		if( !pauseMotion){
            std::cout << rotations[0] << std::endl;
			if(simulationGO){
				motorSpeeds = startTurn();
                motorSpeedPub.publish(motorSpeeds);
				std::cout << "turn motion in progress" << std::endl ;
			}
			else if(turnCompletion){
                simulationGO = !turnCompletion ;
				if(completeMessage){
				    std_msgs::String msg ;
				    msg.data = "I completed the task!\n";
				    completionInfo.publish(msg);
				    std::cout << "turn motion completed "<< std::endl ;
				    completeMessage = false;
			}
             
			}
	}
		// handle ROS messages:
		ros::spinOnce();

		// sleep a bit:
		usleep(1000);

	}
	
	return 0 ;
}