//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>

#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"

#include "vrep_common/JointSetStateData.h"
#include "vrep_common/VrepInfo.h"


#define PI 3.14159265

int direction = 0 ; // -1 is for backwards , 1 is for forwards
float speed = 0.0f ;

bool simulationRunning = true;
float simulationTime=0.0f;

bool isHardwareOk = false ;

float locations[3] = {};
float rotations[3] = {};
float q0,q1,q2,q3 = 0.0;
//First location values of the robot 
float locationF[3] = {} ;
// check variable for initial rotation
bool initialRotations = false ; 
bool initialLocations = true ;
//initial alpha rotation value
float initialRot = 0.0;
float travelledDistance = 0.0f ;
float travelDistanceLength = 0.0f ;

bool ordersTaken = false ;
bool simulationGOm = false ; // m for motion
bool motionCompletion = false ;
bool pauseMotion = false; 

bool completeMessage = true ;

float leftID,rightID,coverID,skirtID = 0.0 ;
//Callback functions
void hardwareCallBack(const std_msgs::Bool::ConstPtr& check){
	isHardwareOk = check->data;
}
void leftMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& left){
	leftID = left->data ;
}
void rightMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& right){
	rightID = right->data ;
}
void coverMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& cover){
	coverID = cover->data ;
}
void skirtMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& skirt){
	skirtID = skirt->data ;

}

void infoCallback(const boost::shared_ptr<vrep_common::VrepInfo>& info){
	simulationTime=info->simulationTime.data;
	simulationRunning = (info->simulatorState.data&1)!=0;
}

void locCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		initialRotations = true ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;
		motionCompletion = false;
		simulationGOm = true ;
		completeMessage = true ;
		initialLocations = true; 
	}

	travelledDistance = std::sqrt(std::pow((locations[0] - locationF[0]),2) + std::pow((locations[1] - locationF[1]),2) +std::pow((locations[2] - locationF[2]),2));
}

void motionInfoCallBack(const std_msgs::Float32MultiArray::ConstPtr& info){
	// if(travelDistanceLength < travelledDistance){
	// 	ordersTaken = false;
	// 	initialLocations = false ;
	// }
	// if(!ordersTaken){
	direction = info->data[0];
	speed = info->data[1];
	travelDistanceLength = info->data[2];
	ordersTaken = true ;
	simulationGOm = true ;
	initialLocations = false ;
	//}
}

vrep_common::JointSetStateData startMotion(){
	vrep_common::JointSetStateData msg; 
	float leftSpeed = speed * direction ;
	float rightSpeed = speed * direction ;
	
	if(travelDistanceLength < travelledDistance) {
		leftSpeed = 0.0 ;
		rightSpeed = 0.0;
		motionCompletion = true ;
	} 

	msg.handles.data.push_back(leftID);
	msg.setModes.data.push_back(2);
	msg.values.data.push_back(leftSpeed);

	msg.handles.data.push_back(rightID);
	msg.setModes.data.push_back(2);
	msg.values.data.push_back(rightSpeed);
	return msg ;

}

void pauseCallBack(const std_msgs::Empty::ConstPtr& info){
	pauseMotion = true ;
}
void resumeCallBack(const std_msgs::Empty::ConstPtr& info){
	pauseMotion = false ;
}
void terminateCallBack(const std_msgs::Empty::ConstPtr& info){
	ordersTaken = false ;
	simulationGOm = false ;
}

int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("straightMotion");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("straightNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS------------------------------------------------------

	ros::Subscriber hardwareInfo = node.subscribe("/sensorControl/hardwareOk",1,hardwareCallBack);

	ros::Subscriber localSub = node.subscribe("/vrep/RobotLocation",1,locCallBack);

	ros::Subscriber straightSub = node.subscribe("/straightDirectives",1,motionInfoCallBack);

	ros::Subscriber pauseInfo = node.subscribe("/controls/pause",1,pauseCallBack);

	ros::Subscriber resumeInfo = node.subscribe("/controls/resume",1,resumeCallBack);

	ros::Subscriber terminateInfo = node.subscribe("/controls/terminate",1,terminateCallBack);

	ros::Subscriber leftMotorIDInfo = node.subscribe("/motorID/left",1,leftMotorIDCallBack);

	ros::Subscriber rightMotorIDInfo = node.subscribe("/motorID/right",1,rightMotorIDCallBack);

	ros::Subscriber coverMotorIDInfo = node.subscribe("/motorID/cover",1,coverMotorIDCallBack);

	ros::Subscriber skirtMotorIDInfo = node.subscribe("/motorID/skirt",1,skirtMotorIDCallBack);

	//ALL PUBLISHERS ------------------------------------------------------

	ros::Publisher completionInfo = node.advertise<std_msgs::String>("CompletedInfo",1);

	ros::Publisher motorSpeedPub=node.advertise<vrep_common::JointSetStateData>("/Motors",1);

	vrep_common::JointSetStateData motorSpeeds ;
	//Simulation main loop
	while(ros::ok && simulationRunning){
		if(!pauseMotion){

			if(simulationGOm){
				vrep_common::JointSetStateData msg ;
			
				msg = startMotion();

				motorSpeedPub.publish(msg);
				std::cout << "straight motion in progress"<< std::endl ;
			
			}
			if(motionCompletion){
				simulationGOm = !motionCompletion ;
				if(completeMessage){
					std_msgs::String msg ;
					msg.data = "I completed the task!\n";
					completionInfo.publish(msg);
					std::cout << "straight motion completed" << std::endl ;
					completeMessage = false;
				}
				
			}

		
	}
		// handle ROS messages:
		ros::spinOnce();

		// sleep a bit:
		usleep(1000);

	}
	
	return 0 ;
}