//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <math.h>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"
#include "geometry_msgs/PoseStamped.h"

// Used API services:
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosReadDistance.h"
#include "vrep_common/simRosEnableSubscriber.h"
 
#define PI 3.14159265

//Global Variables and Publisher subscriber Callbacks

bool simulationRunning = true;
float simulationTime=0.0f;
float x,y,z = 0.0 ; // Sensor data message
//Twist message arrays
float angularData[3] = {};
float linearData[3] = {};

//variables for positional data 
float initialX, initialY , initialZ = 0.0 ;
float xRot, yRot,zRot  = 0.0 ;
float q0,q1,q2,q3 = 0.0;


float detectedDistance = 0;
int objectDistanceHandle = 0;

bool initialPosition = false ;



ros::Timer timeCheck;

void infoCallback(const vrep_common::VrepInfo::ConstPtr& info){
	simulationTime=info->simulationTime.data;
	simulationRunning = (info->simulatorState.data&1)!=0;
}
void timerCallBack(const ros::TimerEvent& event){
	angularData[2] = 0;
	linearData[0] = 0;
	std::cout << "make it zero\n";
	timeCheck.stop();
}

void velocityCallBack(const geometry_msgs::Twist::ConstPtr& info){
	
	if(timeCheck.isValid()){
		timeCheck.stop();
		timeCheck.start();
	}
	angularData[2] = info->angular.z;
	linearData[0] = info->linear.x;
	//std::cout << "first = " << initialZ << " current = " << zRot << std::endl ;
}

void distanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){
	objectDistanceHandle = info->detectedObject.data ;
	x = info->detectedPoint.x ;
	y = info->detectedPoint.y ;
	z = info->detectedPoint.z ;

	float sensorToWall = std::sqrt(std::pow(x,2) + std::pow(y,2) + std::pow(z,2));
	//detectedDistance = std::sqrt(std::pow(x,2) + std::pow(y,2) + std::pow(z,2));
	detectedDistance = (sensorToWall * cos((initialZ - zRot) * PI / 180.0));
//	std::cout << sensorToWall << " " << initialZ << " " << zRot << " " << std::endl ;
	std::cout << "detected object test "<< detectedDistance << std::endl;

}

void locationCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

		q0 =location->pose.orientation.x; 		
		q1 =location->pose.orientation.y;
		q2 =location->pose.orientation.z;
		q3=location->pose.orientation.w;
		xRot = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
		yRot = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
		zRot = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	
}

float CalculateDegree(float initX,float initY, float currX,float currY){

	float input = ((currY- initY) / (currX - initX));

	float result = atan(input) * 180 / PI ;
	return result ;
}



int main(int argc,char* argv[]){
// sensor handle and motor handles
	int leftMotorHandle ;
	int rightMotorHandle;
	int sensorHandle;
	int bodyHandle ;


	if(argc >= 5){

		leftMotorHandle = atoi(argv[1]);
		rightMotorHandle = atoi(argv[2]);
		sensorHandle = atoi(argv[3]);
		bodyHandle = atoi(argv[4]);
	} else {
		std::cout << "Indicate following arguments: leftMotorHandle rightMotorHandle sensorHandle!"<< std::endl;
		sleep(5000);
		return 0 ;
	}

	//Creation of the Ros Node
	int _argc = 0 ;
	char** _argv = NULL ;
	//Randomize ROS Node method from rosBubbleRob
	struct timeval tv;
	unsigned int timeVal=0;
	if (gettimeofday(&tv,NULL)==0)
		timeVal=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
	std::string nodeName("sensorControl");
	// std::string randId(boost::lexical_cast<std::string>(timeVal+int(999999.0f*(rand()/(float)RAND_MAX))));
	// nodeName+=randId;
	// ROS node initialization 
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);
	
	//Create a node handle
	ros::NodeHandle nh("sensorControl");
	timeCheck = nh.createTimer(ros::Duration(1), timerCallBack,false);
	//Subscription to V-REP's info stream
	//SUBSCRIBER FOR V-REP
	ros::Subscriber defaultSub=nh.subscribe("/vrep/info",1,infoCallback);

	//Create a subscriber for the geometry_msgs/twist
	//SUBSCRIBER FOR GEOMETRY_MSGS/TWIST
	ros::Subscriber velocityInfo = nh.subscribe("/wallFollower/velocity",1,velocityCallBack);

	//Create a publisher for the location of the robot
	//PUBLISHER FOR LOCATION OR ROBOT
	ros::ServiceClient clien_locationPublisher=nh.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
	vrep_common::simRosEnablePublisher srv_locationPub ;
	srv_locationPub.request.topicName="RobotLocation";
	srv_locationPub.request.queueSize=1;
	srv_locationPub.request.streamCmd=simros_strmcmd_get_object_pose ;
	srv_locationPub.request.auxInt1=bodyHandle;
	srv_locationPub.request.auxInt2 = -1 ;

	//Create a publisher for the proximity sensor
	//PUBLISHER FOR SENSOR DATA
	ros::ServiceClient client_enablePublisher=nh.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
	vrep_common::simRosEnablePublisher srv_enablePublisher;
	srv_enablePublisher.request.topicName="ProximitySensorData"; // the requested topic name
	srv_enablePublisher.request.queueSize=1; // the requested publisher queue size (on V-REP side)
	srv_enablePublisher.request.streamCmd=simros_strmcmd_read_proximity_sensor; // the requested publisher type
	srv_enablePublisher.request.auxInt1=sensorHandle; // some additional information the publisher needs (what proximity sensor)


	
	 ros::Subscriber sub=nh.subscribe("/vrep/ProximitySensorData",1,distanceCallBack);

	 ros::Subscriber locSub = nh.subscribe("/vrep/RobotLocation",1,locationCallBack);

	 //This publisher sends the relative distance to the detected object to wallFollower node
	 ros::Publisher distancePub = nh.advertise<std_msgs::Float32>("relativeDistance",1);

	 ros::Publisher EulerRotation = nh.advertise<geometry_msgs::Point>("eulerRotation",1);


	 if(clien_locationPublisher.call(srv_locationPub) && srv_locationPub.response.effectiveTopicName.length()!=0){

		if ( (client_enablePublisher.call(srv_enablePublisher)&&(srv_enablePublisher.response.effectiveTopicName.length()!=0) )  ){ 


		//SUBSCRIBER FOR MOTOR SPEED, (FOR V-REP)
		ros::ServiceClient client_enableSubscriber=nh.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
		vrep_common::simRosEnableSubscriber srv_enableSubscriber;
		srv_enableSubscriber.request.topicName="/"+nodeName+"/wheels"; // the topic name
		srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
		srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_state; // the subscriber type

	
		if ( client_enableSubscriber.call(srv_enableSubscriber)&&(srv_enableSubscriber.response.subscriberID!=-1) ){
			// ok, the service call was ok, and the subscriber was succesfully started on V-REP side
			// V-REP is now listening to the desired motor joint states

			//publisher for the V-REP motor speed subscriber
			//PUBLISHER FOR JOINT_SET_DATA (MOTORSPEED)
			ros::Publisher motorSpeedPub=nh.advertise<vrep_common::JointSetStateData>("wheels",1);


			vrep_common::JointSetStateData motorSpeeds;

			float desiredLeftMotorSpeed = 0;
			float desiredRightMotorSpeed =0;


			while (ros::ok()&&simulationRunning)
			{ // this is the control loop

					geometry_msgs::Point EulerMsg ;
					EulerMsg.x = xRot ;
					EulerMsg.y = yRot ;
					EulerMsg.z = zRot ;
					EulerRotation.publish(EulerMsg);

					std_msgs::Float32 msg ;
					msg.data = detectedDistance;
					distancePub.publish(msg);
					
					if(linearData[0] ){
						desiredRightMotorSpeed = linearData[0] + angularData[2] ; 
						desiredLeftMotorSpeed = linearData[0] - angularData[2];
					}else {
					 	desiredRightMotorSpeed = 0 ; 
					 	desiredLeftMotorSpeed = 0;

					
					 }
					
					
				
				motorSpeeds.handles.data.push_back(leftMotorHandle);
				motorSpeeds.handles.data.push_back(rightMotorHandle);
				motorSpeeds.setModes.data.push_back(2); // 2 is the speed mode
				motorSpeeds.setModes.data.push_back(2);
				motorSpeeds.values.data.push_back(desiredLeftMotorSpeed);
				motorSpeeds.values.data.push_back(desiredRightMotorSpeed);
				motorSpeedPub.publish(motorSpeeds);

				// handle ROS messages:
				ros::spinOnce();

				// sleep a bit:
				usleep(5000);
			}
		}
	}
	
	}



	return 0 ;
}
