//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"


// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
//#include "vrep_common/JointSetStateData.h"
#include "vrep_common/simRosEnablePublisher.h"

#define MANOUEVRE_LIMIT 2.0
#define THROTTLE_SPEED 6.0
#define ANGULAR_CONSTANT 0,8 



//Global variables
float angular[3]= {};
float linear[3]= {};

int detectedObj = 0 ;
int detectedObject = 0 ;
float relativeDistance = 0;
bool simulationRunning = true ;
float xLoc,yLoc,zLoc,initial = 0.0 ;
bool initialPosition = false ;
//Callback functions
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info)
{
	simulationRunning=(info->simulatorState.data&1)!=0;
}
void rotationCallBack(const geometry_msgs::Point::ConstPtr& rotation){
	xLoc = rotation->x;
	yLoc = rotation->y;
	zLoc = rotation->z;
	if(!initialPosition){
		initial = rotation->x;
		initialPosition = false ;
	}
}

void sensorCallBack(const std_msgs::Float32::ConstPtr& msg){
	relativeDistance = msg->data ;
	std::cout << "Relative distance =  " << relativeDistance << std::endl;


}



geometry_msgs::Twist manoeuvre(float currentDistance){
	//Create a default message 
	geometry_msgs::Twist msg ;
	//error for the current distance to the obstacle
	float errorHandle = currentDistance - MANOUEVRE_LIMIT ;

	// only appointing linear X and angular Z 
	msg.linear.x = THROTTLE_SPEED;
	if( ((initial - xLoc) < 15 && (initial - xLoc) > -15) ){
	float angularZ = (ANGULAR_CONSTANT * errorHandle);
	msg.angular.z = angularZ;
		return msg ;
	}else{
		msg.angular.z = 0.0;
		return msg ;
	}

	
}

int main(int argc,char* argv[]){

	int _argc = 0;
	char** _argv = NULL;
	struct timeval tv;
	unsigned int timeVal=0;
	if (gettimeofday(&tv,NULL)==0)
		timeVal=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
	std::string nodeName("wallFollower");
	// std::string randId(boost::lexical_cast<std::string>(timeVal+int(999999.0f*(rand()/(float)RAND_MAX))));
	// nodeName+=randId;
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("~");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	ros::Subscriber subInfo=node.subscribe("/vrep/info",1,infoCallback);

	ros::Subscriber sensorInfo = node.subscribe("/sensorControl/relativeDistance",1,sensorCallBack);

	ros::Subscriber rotationInfo = node.subscribe("/sensorControl/eulerRotation",1,rotationCallBack);

	ros::Publisher VelocityPub = node.advertise<geometry_msgs::Twist>("velocity",1);

	while(ros::ok && simulationRunning){

		geometry_msgs::Twist msg = manoeuvre(relativeDistance);
		VelocityPub.publish(msg);


		ros::spinOnce() ;

		usleep(1000);
	}

	return 0 ;
}
