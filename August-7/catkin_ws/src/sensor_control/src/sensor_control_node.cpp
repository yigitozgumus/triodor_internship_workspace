//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <math.h>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"



// Used API services:
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosReadDistance.h"
#include "vrep_common/simRosEnableSubscriber.h"
#include "vrep_common/simRosSetJointTargetPosition.h"

#define PI 3.14159265
//Global Variables and Publisher subscriber Callbacks

bool simulationRunning = true;
float simulationTime=0.0f;

float leftSpeedValue = 0.0 ;
float rightSpeedValue = 0.0 ;


float coverTurnValue = 180 * PI / 180 ;
float skirtTurnValue = 0.0;
float coverTurnSpeed = 0.0;
bool isCoverSet = false;

float x[2],y[2],z[2] = {} ; // Sensor data message

float detectedDistance[2] = {};


ros::Timer timeCheck;

void infoCallback(const boost::shared_ptr<vrep_common::VrepInfo>& info){
	simulationTime=info->simulationTime.data;
	simulationRunning = (info->simulatorState.data&1)!=0;
}
void timerCallBack(const ros::TimerEvent& event){
	leftSpeedValue = 0.0 ;
	rightSpeedValue = 0.0 ;
	std::cout << "make it zero\n";
	timeCheck.stop();
}

void leftVelocityCallBack(const boost::shared_ptr<std_msgs::Float32>& info){

	if(timeCheck.isValid()){
		timeCheck.stop();
		timeCheck.start();
	}

	leftSpeedValue = info->data;
	//std::cout << "first = " << initialZ << " current = " << zRot << std::endl ;
}

void rightVelocityCallBack(const boost::shared_ptr<std_msgs::Float32>& info){

	if(timeCheck.isValid()){
		timeCheck.stop();
		timeCheck.start();
	}

	rightSpeedValue = info->data;
	//std::cout << "first = " << initialZ << " current = " << zRot << std::endl ;
}

void wallInfoCallBack(const boost::shared_ptr<std_msgs::Float32>& info){
	
	 coverTurnValue = (info->data + 155) * PI / 180;
	 
	 coverTurnSpeed = 30.0;

	// std::cout << coverTurnValue << std::endl;
	if(!isCoverSet)
	isCoverSet = true ;
	//std::cout << "Calibrated" << std::endl ;
}


void skirtInfoCallBack(const boost::shared_ptr<std_msgs::Float32>& info){

	skirtTurnValue = info->data ;
}


int main(int argc,char* argv[]){
// sensor handle and motor handles

	int leftMotorHandle ;
	int rightMotorHandle;
	int coverHandle;
	int skirtHandle ;

	if(argc >= 5){

		leftMotorHandle = atoi(argv[1]);
		rightMotorHandle = atoi(argv[2]);
		coverHandle = atoi(argv[3]);
		skirtHandle = atoi(argv[4]);
	} else {
		std::cout << "Indicate following arguments: leftMotorHandle rightMotorHandle sensorHandle!"<< std::endl;
		sleep(5000);
		return 0 ;
	}

	//Creation of the Ros Node
	int _argc = 0 ;
	char** _argv = NULL ;
	std::string nodeName("sensorControl");

	// ROS node initialization
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	//Create a node handle
	ros::NodeHandle nh("sensorControl");
	timeCheck = nh.createTimer(ros::Duration(1), timerCallBack,false);
	//Subscription to V-REP's info stream
	//SUBSCRIBER FOR V-REP
	ros::Subscriber defaultSub=nh.subscribe("/vrep/info",1,infoCallback);

	//Create a subscriber for the geometry_msgs/twist
	//SUBSCRIBER FOR GEOMETRY_MSGS/TWIST
	ros::Subscriber leftVelocityInfo = nh.subscribe("/sensorControl/leftVelocity",1,leftVelocityCallBack);

	ros::Subscriber rightVelocityInfo = nh.subscribe("/sensorControl/rightVelocity",1,rightVelocityCallBack);


	//SUBSCRIBER FOR COVER AND SKIRT MOTOR TURN VALUES

	ros::Subscriber DirectionInfo = nh.subscribe("/sensorControl/coverDirection",1,wallInfoCallBack);

	ros::Subscriber SkirtDirectionInfo = nh.subscribe("/skirtDirection",1,skirtInfoCallBack);

	//PUBLISHER FOR JOINT_SET_DATA (MOTORSPEED)
	ros::Publisher LmotorSpeedPub=nh.advertise<vrep_common::JointSetStateData>("/Motors/left",1);
	 ros::Publisher RmotorSpeedPub=nh.advertise<vrep_common::JointSetStateData>("/Motors/right",1);
	// ros::Publisher SmotorSpeedPub=nh.advertise<vrep_common::JointSetStateData>("/Motors/skirt",1);
	 ros::Publisher CmotorSpeedPub=nh.advertise<vrep_common::JointSetStateData>("/Motors/cover",1);
	//ros::Publisher completeMsg= nh.advertise<std_msgs::Bool>("Permission",1);
	//ros::Publisher coverAnglePub=nh.advertise<std_msgs::Float64>("coverAngle",1);

	ros::Publisher hardwareOk = nh.advertise<std_msgs::Bool>("/sensorControl/hardwareOk",1);

	vrep_common::JointSetStateData leftMotorSpeed;
	vrep_common::JointSetStateData rightMotorSpeed ;
	vrep_common::JointSetStateData skirtMotorSpeed ;
	vrep_common::JointSetStateData coverMotorSpeed ;

	while (ros::ok()&&simulationRunning)
	{ // this is the control loop

		std_msgs::Bool hardwareCheck ;
		hardwareCheck.data = true ;
		hardwareOk.publish(hardwareCheck);


		leftMotorSpeed.handles.data.push_back(leftMotorHandle);
		leftMotorSpeed.setModes.data.push_back(2);
		leftMotorSpeed.values.data.push_back(leftSpeedValue);

		rightMotorSpeed.handles.data.push_back(rightMotorHandle);
		rightMotorSpeed.setModes.data.push_back(2);
		rightMotorSpeed.values.data.push_back(rightSpeedValue);

		skirtMotorSpeed.handles.data.push_back(skirtHandle);
		skirtMotorSpeed.setModes.data.push_back(2);
		skirtMotorSpeed.values.data.push_back(skirtTurnValue);

		coverMotorSpeed.handles.data.push_back(coverHandle);
		coverMotorSpeed.setModes.data.push_back(1);
		coverMotorSpeed.values.data.push_back(coverTurnValue);

		coverMotorSpeed.handles.data.push_back(coverHandle);
		coverMotorSpeed.setModes.data.push_back(2);
		coverMotorSpeed.values.data.push_back(coverTurnSpeed);
		// 2 is the speed mode

		LmotorSpeedPub.publish(leftMotorSpeed);
		RmotorSpeedPub.publish(rightMotorSpeed);
		//LmotorSpeedPub.publish(skirtMotorSpeed);
		CmotorSpeedPub.publish(coverMotorSpeed);

	//	}
		// handle ROS messages:
		ros::spinOnce();

		// sleep a bit:
		usleep(1000);
			
	
		}

	ros::shutdown();
	printf("SensorControlNode just ended!\n");

	return 0 ;
}
