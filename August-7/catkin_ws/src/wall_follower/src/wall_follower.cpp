//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <ros/ros.h>
#include <iostream>
#include <cstdio>

#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
//#include "vrep_common/JointSetStateData.h"
#include "vrep_common/simRosEnablePublisher.h"

#define THROTTLE_SPEED 1.9f
#define PROPORTIONAL_CONSTANT 0.57f
#define DERIVATIVE_CONSTANT 0.052f
#define INTEGRAL_CONSTANT 0.000001f

#define PI 3.14159265

bool completeMessage = true ;
// if it is zero, left sensor data, if it is one, right sensor data


//direction variable for which wall to follow, 0 is left 1 is right 
int direction = 0 ;
float directionAngle = 0.0 ;
// target following distance for the robot
float followingDistance = 0 ;
//total travel distance of the robot
float travelDistanceLength = 0 ;

int detectedObject = 0 ;
float relativeDistance = 0;

//PID control related variables
float previousError = 0.0f;
float totalError = 0.0f ;

//Total travelling distance of the robot
float travelledDistance = 0.0f ;

bool ordersTaken = false ;

float detectedDistance[2] = {};


//Required for Robot Location and rotation calculation 
//initial alpha rotation value
float initialRot = 0.0;
float coverInitialRot = 0.0;
// check variable for initial rotation
bool initialRotations = false ;
bool coverInitialRotations = false ;
//check variable for initial locations
bool initialLocations = true ;
//First location values of the robot 
float locationF[3] = {} ; 
float locations[3] = {};
float coverRotations[3] = {};
float rotations[3] = {};
float coverQ0,coverQ1,coverQ2,coverQ3 = 0.0;
float q0,q1,q2,q3 = 0.0;
float sensorToWall[2]={};

bool pauseMotion = false; 

bool simulationGO = false ;

bool isHardwareOk = false ;

bool followCompletion = false ;

//Callback functions

void sensorCallBack(const boost::shared_ptr<std_msgs::Float32>& msg){
	relativeDistance = msg->data ;
//	std::cout << "Relative distance =  " << relativeDistance << std::endl;
}

void leftDistanceCallBack(const boost::shared_ptr<std_msgs::Float32>& distance){
	sensorToWall[0] = distance->data ;
	detectedDistance[0]   = (sensorToWall[0] * cos((rotations[0] - initialRot) * PI / 180.0));
	//std::cout <<"Left detected =  "<< detectedDistance[0]  << std::endl ;
}

void rightDistanceCallBack(const boost::shared_ptr<std_msgs::Float32>& distance){
	sensorToWall[1] = distance->data ;
	detectedDistance[1]  = (sensorToWall[1] * cos((rotations[0] - initialRot) * PI / 180.0));
	std::cout <<"Right detected =  "<< detectedDistance[1]  << std::endl ;
}


void locationCallBack(const boost::shared_ptr<geometry_msgs::PoseStamped>& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		initialRotations = true ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;
		followCompletion = false ;
        completeMessage = true ;
        simulationGO = true ;
        initialLocations = true; 
	}

	travelledDistance = std::sqrt(std::pow((locations[0] - locationF[0]),2) + std::pow((locations[1] - locationF[1]),2) );
}

void coverRotationCallBack(const boost::shared_ptr<geometry_msgs::PoseStamped>& location){

    coverQ0 =location->pose.orientation.x;       
    coverQ1 =location->pose.orientation.y;
    coverQ2 =location->pose.orientation.z;
    coverQ3=location->pose.orientation.w;
    coverRotations[0] = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
    coverRotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
    coverRotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
    //std::cout << rotations[0] - initialRot  << std::endl ;
    if(!coverInitialRotations){
        coverInitialRot = coverRotations[2];
        coverInitialRotations = true ;
    }
    
}

void directiveCallBack(const boost::shared_ptr<std_msgs::Float32MultiArray>& directives){

		// if(followCompletion){
		// 	ordersTaken = false;
		// 	initialLocations = false ;
		// }
		// if(!ordersTaken){
			
			direction = directives->data[0];
			followingDistance = directives->data[1];
			travelDistanceLength = directives->data[2];
            coverInitialRotations = false;
			ordersTaken = true ;
			//simulationGO = true;
            initialLocations =false;
          
	//	}

		
}

void pauseCallBack(const boost::shared_ptr<std_msgs::Empty>& info){
	pauseMotion = true ;
}
void resumeCallBack(const boost::shared_ptr<std_msgs::Empty>& info){
	pauseMotion = false ;
}
void terminateCallBack(const boost::shared_ptr<std_msgs::Empty>& info){
	ordersTaken = false ;
	simulationGO = false ;
}
void hardwareCallBack(const boost::shared_ptr<std_msgs::Bool>& check){
	isHardwareOk = check->data;
}



std_msgs::Float32MultiArray manoeuvre(float currentDistance){
	//Create a default message 
	std_msgs::Float32MultiArray msg ;
	//error for the current distance to the obstacle
	float errorHandle = currentDistance - followingDistance ;
	totalError += errorHandle ;
	float errorC_P = errorHandle - previousError ;

	float manouevreValue = (PROPORTIONAL_CONSTANT * errorHandle) +
							 (DERIVATIVE_CONSTANT * errorC_P) + 
							 (INTEGRAL_CONSTANT * totalError) ;

	previousError = errorHandle ;

	float leftSpeed = 0;
	float rightSpeed =0;

	if(currentDistance > followingDistance){
		leftSpeed = THROTTLE_SPEED + (manouevreValue * (direction));
		rightSpeed = THROTTLE_SPEED + (manouevreValue * (1 - direction) ) ;
		
	}else{	
		leftSpeed =THROTTLE_SPEED - (manouevreValue * (1 - direction));
		rightSpeed=THROTTLE_SPEED - (manouevreValue * (direction )) ;
		
	}
	if(  (rotations[0] - initialRot) < -15 && (leftSpeed > rightSpeed) ){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;
		
	}else if( (rotations[0] - initialRot) > 15 && ( rightSpeed > leftSpeed )){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;	
			
		}

	if(travelDistanceLength < travelledDistance ) {
		leftSpeed = 0.0 ;
		rightSpeed = 0.0;
		followCompletion = true ;
	}

	msg.data.push_back(leftSpeed);
	msg.data.push_back(rightSpeed);
	return msg ;
			
}

int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("wallFollower");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("WallNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS----------------------------------------------------------------------------

	//Default info callback 

	ros::Subscriber leftDistInfo = node.subscribe("/vrep/leftSensorDistance",1,leftDistanceCallBack);

	ros::Subscriber rightDistInfo = node.subscribe("/vrep/rightSensorDistance",1,rightDistanceCallBack);
	
	ros::Subscriber locSub = node.subscribe("/vrep/RobotLocation",1,locationCallBack);

    ros::Subscriber coverSub = node.subscribe("/vrep/CoverRotation",1,coverRotationCallBack);

	
	// Listens which wall to follow
	ros::Subscriber directiveInfo = node.subscribe("/followDirectives",1,directiveCallBack);

	ros::Subscriber hardwareInfo = node.subscribe("/sensorControl/hardwareOk",1,hardwareCallBack);

	ros::Subscriber pauseInfo = node.subscribe("/controls/pause",1,pauseCallBack);

	ros::Subscriber resumeInfo = node.subscribe("/controls/resume",1,resumeCallBack);

	ros::Subscriber terminateInfo = node.subscribe("/controls/terminate",1,terminateCallBack);


	//ALL PUBLISHERS----------------------------------------------------------------------------------

	ros::Publisher leftVelocityPub = node.advertise<std_msgs::Float32>("/sensorControl/leftVelocity",1);

	ros::Publisher rightVelocityPub = node.advertise<std_msgs::Float32>("/sensorControl/rightVelocity",1);

	ros::Publisher DirectionInfo = node.advertise<std_msgs::Float32>("/sensorControl/coverDirection",1);

	ros::Publisher completionInfo = node.advertise<std_msgs::String>("CompletedInfo",1);


	//Simulation main loop
	while(ros::ok ){

	if(isHardwareOk && !pauseMotion){

		//TODO
		// this if block and the simulationGO can be merged
		// things to consider, directive lock when in progress
		if(ordersTaken){
		std_msgs::Float32 initial ;
		if(direction){
				directionAngle = -25;
			}else{
				directionAngle = 25 ;
			}
		initial.data = directionAngle ;
		DirectionInfo.publish(initial);
		ordersTaken = false; 
		}
       
		//continous task support
		//simulationGO = !followCompletion;
	  std::cout << coverRotations[2]  << "||"
      <<coverRotations[1] << "||"
      <<coverRotations[0]  << std::endl ; 
	if(simulationGO ){	
		
		std_msgs::Float32 leftM ;
		std_msgs::Float32 rightM ;

		std_msgs::Float32MultiArray msg ;
		msg = manoeuvre(detectedDistance[direction]);

		 leftM.data = msg.data[0];
		 rightM.data = msg.data[1];
		 
		leftVelocityPub.publish(leftM);
		rightVelocityPub.publish(rightM);
		//std::cout << "follow task in progress"<< std::endl ;
		}
			//TODO 
			//very bad coding ?
		if(followCompletion) {
		if(completeMessage){
			std_msgs::String msg ;
			msg.data = "I completed the task!\n";
			completionInfo.publish(msg);
		//	std::cout << "follow task completed" << std::endl ;
			completeMessage = false;
		}
		simulationGO = !followCompletion;
		}

		
	}
	
		ros::spinOnce() ;

		usleep(1000);
	}
	
	return 0 ;
}
