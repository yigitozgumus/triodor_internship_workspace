// include files are the same with the Bubble_Rob node since their functionalities will be same
#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include "../include/v_repConst.h"

// Used data structures:

#include "vrep_common/ProximitySensorData.h"// added but will not be used
#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"

// Used API services:
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosEnableSubscriber.h"

bool simulationRunning=true;
bool sensorTrigger=false;
float simulationTime=0.0f;

// First Global function is the same but sensorcallback function is unnecessary
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info)
{
	simulationTime=info->simulationTime.data;
	simulationRunning=(info->simulatorState.data&1)!=0;
}

int main(int argc,char* argv[])
{
	// The joint handles and proximity sensor handles are given in the argument list
	// (when V-REP launches this executable, V-REP will also provide the argument list)
	int leftMotorHandle;
	int rightMotorHandle;
	if (argc>=3)
	{
		leftMotorHandle=atoi(argv[1]);
		rightMotorHandle=atoi(argv[2]);
	}
	else
	{
		printf("Indicate following arguments: 'leftMotorHandle rightMotorHandle sensorHandle'!\n");
		sleep(5000);
		return 0;
	}

	int _argc = 0;
	char** _argv = NULL;
	std::string name_node("Ros_Control_Node");
	ros::init(_argc,_argv,name_node.c_str());

	if(!ros::master::check())
			return(0);

	ros::NodeHandle node("~");
	printf("rosControlTest just started with node name %s\n",name_node.c_str());

// 1. Let's subscribe to V-REP's info stream (that stream is the only one enabled by default,
	// and the only one that can run while no simulation is running):
	ros::Subscriber subInfo=node.subscribe("/vrep/info",1,infoCallback);

	// 4. Let's tell V-REP to subscribe to the motor speed topic (publisher to that topic will be created further down):
		ros::ServiceClient client_enableSubscriber=node.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
		vrep_common::simRosEnableSubscriber srv_enableSubscriber;

	srv_enableSubscriber.request.topicName="/"+name_node+"/wheels"; // the topic name
		srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
		srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_state; // the subscriber type

	if ( client_enableSubscriber.call(srv_enableSubscriber)&&(srv_enableSubscriber.response.subscriberID!=-1) )
		{	// ok, the service call was ok, and the subscriber was succesfully started on V-REP side
			// V-REP is now listening to the desired motor joint states

			// 5. Let's prepare a publisher of those motor speeds:
			ros::Publisher motorSpeedPub=node.advertise<vrep_common::JointSetStateData>("wheels",1);

	float driveBackStartTime=-99.0f;
			while (ros::ok()&&simulationRunning)
			{ // this is the control loop (very simple, just as an example)
				vrep_common::JointSetStateData motorSpeeds;
				float desiredLeftMotorSpeed;
				float desiredRightMotorSpeed;
				
				// going forward:
					desiredLeftMotorSpeed=-3.1415;
					desiredRightMotorSpeed=3.1415;
					desiredLeftMotorSpeed=0;
					desiredRightMotorSpeed=0;
					
				// publish the motor speeds:
				motorSpeeds.handles.data.push_back(leftMotorHandle);
				motorSpeeds.handles.data.push_back(rightMotorHandle);
				motorSpeeds.setModes.data.push_back(2); // 2 is the speed mode
				motorSpeeds.setModes.data.push_back(2);
				motorSpeeds.values.data.push_back(desiredLeftMotorSpeed);
				motorSpeeds.values.data.push_back(desiredRightMotorSpeed);
				motorSpeedPub.publish(motorSpeeds);

				// handle ROS messages:
				ros::spinOnce();

				// sleep a bit:
				usleep(5000);
			}
	}
	ros::shutdown();
	printf("rosBubbleRob just ended!\n");
	return(0);
}




