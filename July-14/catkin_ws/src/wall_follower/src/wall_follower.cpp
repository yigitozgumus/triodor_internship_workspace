//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
//#include "vrep_common/JointSetStateData.h"
#include "vrep_common/simRosEnablePublisher.h"



//Global variables
float angular[3]= {};
float linear[3]= {};
float detected[4] = {};
float normalV[3]= {};
int detectedObj = 0 ;
int detectedObject = 0 ;

bool simulationRunning = true ;
//Callback functions
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info)
{
	simulationRunning=(info->simulatorState.data&1)!=0;
}
void twistCallBack(const geometry_msgs::Twist::ConstPtr& velocity){


}

void sensorCallBack(const vrep_common::ProximitySensorData::ConstPtr& sens){
	detected[0] = sens->detectedPoint.x;std::cout << detected[0] << std::endl;	
	detected[1] = sens->detectedPoint.y;std::cout << detected[1] << std::endl;
	detected[2] = sens->detectedPoint.z;std::cout << detected[2] << std::endl;
	detectedObject = sens->detectedObject.data;
	normalV[0] = sens->normalVector.x;std::cout << normalV[0] << std::endl;
	normalV[1] = sens->normalVector.y;std::cout << normalV[1] << std::endl;
	normalV[2] = sens->normalVector.z;std::cout << normalV[2] << std::endl;
	std::cout << "---------------------------------------------------------------------" << std::endl;
	detectedObj = sens->detectedObject.data;

}

int main(int argc,char* argv[]){

	int _argc = 0;
	char** _argv = NULL;
	struct timeval tv;
	unsigned int timeVal=0;
	if (gettimeofday(&tv,NULL)==0)
		timeVal=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
	std::string nodeName("wallFollower");
	// std::string randId(boost::lexical_cast<std::string>(timeVal+int(999999.0f*(rand()/(float)RAND_MAX))));
	// nodeName+=randId;
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("~");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	ros::Subscriber subInfo=node.subscribe("/vrep/info",1,infoCallback);

	ros::Subscriber sensorInfo = node.subscribe("/vrep/ProximitySensorData",1,sensorCallBack);



	ros::Publisher twister = node.advertise<geometry_msgs::Twist>("velocity",1);

	while(ros::ok && simulationRunning){

		ros::spinOnce() ;

		usleep(1000);
	}

	return 0 ;
}
