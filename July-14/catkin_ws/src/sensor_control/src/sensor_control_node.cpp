//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"

// Used API services:
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosReadDistance.h"
#include "vrep_common/simRosEnableSubscriber.h"

//Global Variables and Publisher subscriber Callbacks

bool simulationRunning = true;
float simulationTime=0.0f;
float x,y,z = 0.0 ; // Sensor data message
//Twist message arrays
float angularData[3] = {};
float linearData[3] = {};

int objectDistanceHandle ;



ros::Timer timeCheck;

void infoCallback(const vrep_common::VrepInfo::ConstPtr& info){
	simulationTime=info->simulationTime.data;
	simulationRunning = (info->simulatorState.data&1)!=0;
}
void timerCallBack(const ros::TimerEvent& event){
	angularData[2] = 0;
	linearData[0] = 0;
	std::cout << "make it zero\n";
	timeCheck.stop();
}

void velocityCallBack(const geometry_msgs::Twist::ConstPtr& info){
	
	if(timeCheck.isValid()){
		timeCheck.stop();
		timeCheck.start();
	}
	angularData[2] = info->angular.z;
	linearData[0] = info->linear.x;
}

void distanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){
	objectDistanceHandle = info->detectedObject.data ;
	
	//std::cout << "detected object test"<< std::endl;
}



int main(int argc,char* argv[]){
// sensor handle and motor handles
	int leftMotorHandle ;
	int rightMotorHandle;
	int sensorHandle;
	int bodyHandle ;

	if(argc >= 5){

		leftMotorHandle = atoi(argv[1]);
		rightMotorHandle = atoi(argv[2]);
		sensorHandle = atoi(argv[3]);
		bodyHandle = atoi(argv[4]);
	} else {
		std::cout << "Indicate following arguments: leftMotorHandle rightMotorHandle sensorHandle!"<< std::endl;
		sleep(5000);
		return 0 ;
	}

	//Creation of the Ros Node
	int _argc = 0 ;
	char** _argv = NULL ;
	//Randomize ROS Node method from rosBubbleRob
	struct timeval tv;
	unsigned int timeVal=0;
	if (gettimeofday(&tv,NULL)==0)
		timeVal=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
	std::string nodeName("sensorControl");
	// std::string randId(boost::lexical_cast<std::string>(timeVal+int(999999.0f*(rand()/(float)RAND_MAX))));
	// nodeName+=randId;
	// ROS node initialization 
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);
	
	//Create a node handle
	ros::NodeHandle nh("sensorControl");
	timeCheck = nh.createTimer(ros::Duration(1), timerCallBack,false);
	//Subscription to V-REP's info stream
	//SUBSCRIBER FOR V-REP
	ros::Subscriber defaultSub=nh.subscribe("/vrep/info",1,infoCallback);

	//Create a subscriber for the geometry_msgs/twist
	//SUBSCRIBER FOR GEOMETRY_MSGS/TWIST
	ros::Subscriber velocityInfo = nh.subscribe("/wallFollower/velocity",1,velocityCallBack);

	//Create a publisher for the proximity sensor
	//PUBLISHER FOR SENSOR DATA
	ros::ServiceClient client_enablePublisher=nh.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
	vrep_common::simRosEnablePublisher srv_enablePublisher;
	srv_enablePublisher.request.topicName="ProximitySensorData"; // the requested topic name
	srv_enablePublisher.request.queueSize=1; // the requested publisher queue size (on V-REP side)
	srv_enablePublisher.request.streamCmd=simros_strmcmd_read_proximity_sensor; // the requested publisher type
	srv_enablePublisher.request.auxInt1=sensorHandle; // some additional information the publisher needs (what proximity sensor)

	ros::Subscriber sub=nh.subscribe("/vrep/ProximitySensorData",1,distanceCallBack);

		

	if ( (client_enablePublisher.call(srv_enablePublisher)&&(srv_enablePublisher.response.effectiveTopicName.length()!=0) )  ){ 

		

	
		 // ros::ServiceClient distance_publisher = nh.serviceClient<vrep_common::simRosReadDistance>("/vrep/simRosReadDistance");
		 // vrep_common::simRosReadDistance srv_distancePublisher ;
		 // srv_distancePublisher.request.handle = objectDistanceHandle ;

		// if(distance_publisher.call(srv_distancePublisher)){

		// // ros::ServiceClient client_enablePublisher2=nh.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
		//  vrep_common::simRosEnablePublisher srv_enablePublisher2;
		// srv_enablePublisher2.request.topicName="/std_msgs/float32"; // the requested topic name
		// srv_enablePublisher2.request.queueSize=1; // the requested publisher queue size (on V-REP side)
		// srv_enablePublisher2.request.streamCmd=simros_strmcmd_read_distance; // the requested publisher type
		// srv_enablePublisher2.request.auxInt1=bodyHandle; // some additional information the publisher needs (what proximity sensor)
		// srv_enablePublisher2.request.auxInt2= -1 ;
		// srv_enablePublisher2.request.auxString = "";

		//   if ( client_enablePublisher.call(srv_enablePublisher2)&&(srv_enablePublisher2.response.effectiveTopicName.length()!=0) ){ 

		//Create a subscription for motor control for V-REP

		//SUBSCRIBER FOR MOTOR SPEED, (FOR V-REP)
		ros::ServiceClient client_enableSubscriber=nh.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
		vrep_common::simRosEnableSubscriber srv_enableSubscriber;
		srv_enableSubscriber.request.topicName="/"+nodeName+"/wheels"; // the topic name
		srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
		srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_state; // the subscriber type

	
		if ( client_enableSubscriber.call(srv_enableSubscriber)&&(srv_enableSubscriber.response.subscriberID!=-1) )
		{	// ok, the service call was ok, and the subscriber was succesfully started on V-REP side
			// V-REP is now listening to the desired motor joint states

			//publisher for the V-REP motor speed subscriber
			//PUBLISHER FOR JOINT_SET_DATA (MOTORSPEED)
			ros::Publisher motorSpeedPub=nh.advertise<vrep_common::JointSetStateData>("wheels",1);


			vrep_common::JointSetStateData motorSpeeds;

			float desiredLeftMotorSpeed = 0;
			float desiredRightMotorSpeed =0;


			while (ros::ok()&&simulationRunning)
			{ // this is the control loop

	
					if(linearData[0] ==2){
						desiredLeftMotorSpeed = 7;
						desiredRightMotorSpeed = 7;
						
						std::cout << "moving forward\n";
					
						
					}
					else if(linearData[0] == -2){
						desiredLeftMotorSpeed = -4;
						desiredRightMotorSpeed = -4;
						
						std::cout << "moving back\n";
					
																
					}
					else if(angularData[2] == 2){
						desiredLeftMotorSpeed = -4;
						desiredRightMotorSpeed = +4;
						
						std::cout << "turning left\n";
						
						
					}
					else if(angularData[2] == -2){
						desiredLeftMotorSpeed = 4;
						desiredRightMotorSpeed = -4;
						
						std::cout << "turning right\n";
					
					
					}else{
						desiredLeftMotorSpeed = 0;
						desiredRightMotorSpeed = 0;
					}
				
				motorSpeeds.handles.data.push_back(leftMotorHandle);
				motorSpeeds.handles.data.push_back(rightMotorHandle);
				motorSpeeds.setModes.data.push_back(2); // 2 is the speed mode
				motorSpeeds.setModes.data.push_back(2);
				motorSpeeds.values.data.push_back(desiredLeftMotorSpeed);
				motorSpeeds.values.data.push_back(desiredRightMotorSpeed);
				motorSpeedPub.publish(motorSpeeds);

				// handle ROS messages:
				ros::spinOnce();

				// sleep a bit:
				usleep(5000);
			}
		}
	//}
	
	}



	return 0 ;
}
