# CMake generated Testfile for 
# Source directory: /home/yigit/catkin_ws/src
# Build directory: /home/yigit/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(control_unit)
SUBDIRS(keyboard_control)
SUBDIRS(ros_bubble_rob)
SUBDIRS(sensor_control)
SUBDIRS(wall_follower)
