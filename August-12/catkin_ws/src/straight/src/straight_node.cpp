//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>

#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"

#include "vrep_common/JointSetStateData.h"
#include "vrep_common/VrepInfo.h"
#include "straight/Status.h"

#define PI 3.14159265

int direction = 0 ; // -1 is for backwards , 1 is for forwards
float speed = 0.0f ;

float locations[3] = {};
float rotations[3] = {};
float q0,q1,q2,q3 = 0.0;
//First location values of the robot 
float locationF[3] = {} ;
// check variable for initial rotation
bool initialRotations = false ; 
bool initialLocations = true ;
//initial alpha rotation value
float initialRot = 0.0;
float travelledDistance = 0.0f ;
float travelDistanceLength = 0.0f ;

bool ordersTaken = false ;
bool simulationGOm = false ; // m for motion
bool motionCompletion = false ;
bool pauseMotion = false; 

bool completeMessage = false ;

float leftM,rightM = 0.0 ;
//Callback functions

void locCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		initialRotations = true ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;
		completeMessage = false;
		simulationGOm = true ;
		initialLocations = true; 
	}

	travelledDistance = std::sqrt(std::pow((locations[0] - locationF[0]),2) + std::pow((locations[1] - locationF[1]),2) +std::pow((locations[2] - locationF[2]),2));
}

void motionInfoCallBack(const std_msgs::Float32MultiArray::ConstPtr& info){
	
	direction = info->data[0];
	speed = info->data[1];
	travelDistanceLength = info->data[2];
	ordersTaken = true ;
	simulationGOm = true ;
	initialLocations = false ;
	
}

std_msgs::Float64MultiArray startMotion(){
	std_msgs::Float64MultiArray msg; 
	float leftSpeed = speed * direction ;
	float rightSpeed = speed * direction ;
	
	if(travelDistanceLength < travelledDistance) {
		leftSpeed = 0.0 ;
		rightSpeed = 0.0;
		completeMessage = true ;
		simulationGOm = false;
	} 
	msg.data.push_back(leftSpeed);
	msg.data.push_back(rightSpeed);
	return msg ;

}

void pauseCallBack(const std_msgs::Empty::ConstPtr& info){
	pauseMotion = true ;
}
void resumeCallBack(const std_msgs::Empty::ConstPtr& info){
	pauseMotion = false ;
}
void terminateCallBack(const std_msgs::Empty::ConstPtr& info){
	ordersTaken = false ;
	simulationGOm = false ;
}

bool nodeStatusCallBack(straight::Status::Request& req,straight::Status::Response& res){
	res.status = simulationGOm ;
	ROS_INFO("Sending status of the Node");
	return true ;
}

int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("straightMotion");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("straightNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS------------------------------------------------------

	ros::Subscriber localSub = node.subscribe("/vrep/RobotLocation",1,locCallBack);

	ros::Subscriber straightSub = node.subscribe("/straightDirectives",1,motionInfoCallBack);

	ros::Subscriber pauseInfo = node.subscribe("/controls/pause",1,pauseCallBack);

	ros::Subscriber resumeInfo = node.subscribe("/controls/resume",1,resumeCallBack);

	ros::Subscriber terminateInfo = node.subscribe("/controls/terminate",1,terminateCallBack);

	ros::ServiceServer nodeService = node.advertiseService("/straightNodeStatus", nodeStatusCallBack);
	//ALL PUBLISHERS ------------------------------------------------------

	ros::Publisher completionInfo = node.advertise<std_msgs::Bool>("/controls/completed",1,true);

	ros::Publisher LmotorSpeedPub=node.advertise<std_msgs::Float64>("/Motors/left",1);
	ros::Publisher RmotorSpeedPub=node.advertise<std_msgs::Float64>("/Motors/right",1);

	std_msgs::Float64MultiArray motorSpeeds ;
	std_msgs::Float64 leftMotor;
	std_msgs::Float64 rightMotor ;
	//Simulation main loop
	while(ros::ok()){
		if(!pauseMotion){

			if(simulationGOm){
				motorSpeeds = startMotion();
				leftMotor.data = motorSpeeds.data[0];
				rightMotor.data = motorSpeeds.data[1];
				LmotorSpeedPub.publish(leftMotor);
				RmotorSpeedPub.publish(rightMotor);
				std::cout << "straight motion in progress"<< std::endl ;
			
			}
				if(completeMessage){
					std_msgs::Bool msg;
					msg.data = true;
					completionInfo.publish(msg);
					std::cout << "straight motion completed" << std::endl ;
					completeMessage = false;
				}
				
			
	}
		// handle ROS messages:
		ros::spinOnce();

		// sleep a bit:
		usleep(1000);

	}
	ros::shutdown();
	return 0 ;
}