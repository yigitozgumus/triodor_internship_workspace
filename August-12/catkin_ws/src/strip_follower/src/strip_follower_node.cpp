//AK 10.08.15

#include <iostream>
#include <ros/ros.h>
#include <cmath>
#include <vector>
#include <ctime>

//Messages
#include "std_msgs/String.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Empty.h"
#include "geometry_msgs/PoseStamped.h"
#include "vrep_common/JointSetStateData.h"

#define PI 3.141592653589f

#define PROPORTIONAL_CONSTANT 0.80f
#define DERIVATIVE_CONSTANT 0.052f
#define INTEGRAL_CONSTANT 0.000001f

#define SEARCH_UNIT_TIME 3 //sec
#define SEARCH_BASE_SPEED 1.0f //wheel speed

bool pauseNode;
bool terminateAction;
bool actionflag;
bool hardwareStatus;
bool lastseenStripSide; // 0 left - 1 right  --- use only when strip lost

float previousLeftInductiveSensorDataArrived;
float previousRightInductiveSensorDataArrived;
float leftInductiveSensorData;
float rightInductiveSensorData;
float travel_distance;
float desired_speed;
float feedbackValue;
float leftMotorID;
float rightMotorID;

ros::Publisher pubMotorSpeeds;

//For PID
float totalError;
float previousError;

struct robotPosition{
	float x;
	float y;
	float z;
	float angular_position;
} currentRobotPosition, startedRobotPosition;

// give distance from start point and current point
float getDistance(){
	return sqrtf(pow((currentRobotPosition.x-startedRobotPosition.x),2.0)+pow((currentRobotPosition.y-startedRobotPosition.y),2.0));
}


//callbacks
void hardwareCallBack(const std_msgs::Bool::ConstPtr& hardware){
	hardwareStatus = hardware->data;
}

void pauseCallBack(const std_msgs::Empty::ConstPtr& hit){
	if(actionflag){
		std::cout<<"Strip Follower  --- Paused"<<std::endl;
		pauseNode = true;
	}
}

void resumeCallBack(const std_msgs::Empty::ConstPtr& hit){
	if(pauseNode){
		std::cout<<"Strip Follower  --- Resumed"<<std::endl;
		pauseNode = false;
	}
}

void terminateCallBack(const std_msgs::Empty::ConstPtr& hit){
	terminateAction = true;
	travel_distance = 0;
}

void leftInductiveSensorCallBack(const std_msgs::Float32::ConstPtr& sensorData){
	previousLeftInductiveSensorDataArrived = leftInductiveSensorData;
	leftInductiveSensorData = sensorData->data * 1000;
} 

void rightInductiveSensorCallBack(const std_msgs::Float32::ConstPtr& sensorData){
	previousRightInductiveSensorDataArrived = rightInductiveSensorData;
	rightInductiveSensorData = sensorData->data * 1000;
} 

void locationCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){
	
	float q0 =location->pose.orientation.x; 		
	float q1 =location->pose.orientation.y;
	float q2 =location->pose.orientation.z;
	float q3=location->pose.orientation.w;
	
	currentRobotPosition.angular_position =  atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	
	currentRobotPosition.x=location->pose.position.x;
	currentRobotPosition.y=location->pose.position.y;
	currentRobotPosition.z=location->pose.position.z;
	
}

void directivesCallBack(const std_msgs::Float32::ConstPtr& orders){
	actionflag = true;
	travel_distance = orders->data;
	startedRobotPosition = currentRobotPosition;
	std::cout<<"Strip Follower  --- Started"<<std::endl;
	terminateAction = false;
}

void leftMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& left){
	leftMotorID = left->data ;
}
void rightMotorIDCallBack(const boost::shared_ptr<std_msgs::Float32>& right){
	rightMotorID = right->data ;
}


// PID control system for strip follow
void motorSpeedCalculator(vrep_common::JointSetStateData& motorSpeeds){
	float leftSpeed;
	float rightSpeed;
	float sensorDataDifference = leftInductiveSensorData - rightInductiveSensorData ;
	totalError += sensorDataDifference ;
	float errorDifference = sensorDataDifference - previousError ;

	feedbackValue = (PROPORTIONAL_CONSTANT * sensorDataDifference) +
						  (INTEGRAL_CONSTANT * totalError) +
						  (DERIVATIVE_CONSTANT * errorDifference) ;
	
	previousError = sensorDataDifference;
	if(previousLeftInductiveSensorDataArrived != leftInductiveSensorData && previousRightInductiveSensorDataArrived == rightInductiveSensorData){
		leftSpeed  = desired_speed - 0.5;
		rightSpeed = desired_speed;
	}
	
	else if(previousLeftInductiveSensorDataArrived == leftInductiveSensorData && previousRightInductiveSensorDataArrived != rightInductiveSensorData){
		leftSpeed  = desired_speed;
		rightSpeed = desired_speed - 0.5;
	}
	else{
		
		leftSpeed  = desired_speed - ((sensorDataDifference<0) ? -feedbackValue : 0 );
		rightSpeed = desired_speed - ((sensorDataDifference>0) ?  feedbackValue : 0 );
	}
		
	motorSpeeds.handles.data.push_back(leftMotorID);
	motorSpeeds.setModes.data.push_back(2);
	motorSpeeds.values.data.push_back(leftSpeed);

	motorSpeeds.handles.data.push_back(rightMotorID);
	motorSpeeds.setModes.data.push_back(2);
	motorSpeeds.values.data.push_back(rightSpeed);
	
	
}

bool stripStatus(){ //true if strip lost
	if(previousLeftInductiveSensorDataArrived != leftInductiveSensorData && previousRightInductiveSensorDataArrived == rightInductiveSensorData){
		lastseenStripSide=false;
	}
	else if(previousLeftInductiveSensorDataArrived == leftInductiveSensorData && previousRightInductiveSensorDataArrived != rightInductiveSensorData){
		lastseenStripSide=true;
	}
	
	return (previousLeftInductiveSensorDataArrived == leftInductiveSensorData && previousRightInductiveSensorDataArrived == rightInductiveSensorData);
}

void searchStrip(){
	std::cout<<"Strip Follower  --- Searching the strip"<<std::endl;
	
	bool found = false;
	float motorData;
	time_t currenttime;
	time_t targettime;

	ros::spinOnce();
	for(int i=0 ; i<3 ; i++){
		
		time(&currenttime);
		targettime=currenttime+SEARCH_UNIT_TIME;
		
		std::cout<<currenttime<<"  "<<targettime<<"  -- "<< (stripStatus() && currenttime<targettime)<<std::endl;
		
		while(ros::ok() && stripStatus() && currenttime<targettime)//turn
		{
			vrep_common::JointSetStateData motor;
		
			motor.handles.data.push_back(leftMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(((!lastseenStripSide) ? -SEARCH_BASE_SPEED : SEARCH_BASE_SPEED));
			
			motor.handles.data.push_back(rightMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(((!lastseenStripSide) ? SEARCH_BASE_SPEED : -SEARCH_BASE_SPEED));
			
			pubMotorSpeeds.publish(motor);
			
			ros::spinOnce();
			time(&currenttime);	
		}
		
		
		
		time(&currenttime);
		targettime=currenttime+SEARCH_UNIT_TIME+SEARCH_UNIT_TIME;
		
		while(ros::ok() && stripStatus() && currenttime<targettime)//turn other side
		{
			vrep_common::JointSetStateData motor;
		
			motor.handles.data.push_back(leftMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(((!lastseenStripSide) ? SEARCH_BASE_SPEED : -SEARCH_BASE_SPEED));
			
			motor.handles.data.push_back(rightMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(((!lastseenStripSide) ? -SEARCH_BASE_SPEED : SEARCH_BASE_SPEED));
			
			pubMotorSpeeds.publish(motor);
			
			ros::spinOnce();
			time(&currenttime);
		}
		
		time(&currenttime);
		targettime=currenttime+SEARCH_UNIT_TIME;
		
		while(ros::ok() && stripStatus() && currenttime<targettime)//turn other side
		{
			vrep_common::JointSetStateData motor;
		
			motor.handles.data.push_back(leftMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(((!lastseenStripSide) ? -SEARCH_BASE_SPEED : SEARCH_BASE_SPEED));
			
			motor.handles.data.push_back(rightMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(((!lastseenStripSide) ? SEARCH_BASE_SPEED : -SEARCH_BASE_SPEED));
			
			pubMotorSpeeds.publish(motor);
			
			ros::spinOnce();
			time(&currenttime);	
		}
		
		time(&currenttime);
		targettime=currenttime+SEARCH_UNIT_TIME;
		
		while(ros::ok() && stripStatus() && currenttime<targettime)//backward
		{
			vrep_common::JointSetStateData motor;
		
			motor.handles.data.push_back(leftMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(SEARCH_BASE_SPEED);
			
			motor.handles.data.push_back(rightMotorID);
			motor.setModes.data.push_back(2);
			motor.values.data.push_back(SEARCH_BASE_SPEED);
			
			pubMotorSpeeds.publish(motor);

			ros::spinOnce();	
			time(&currenttime);
		}
		if(!stripStatus()){
			std::cout<<"Strip Follower  --- Strip have been found"<<std::endl;
			return;
		}
	}
	std::cout<<"Strip Follower  --- Strip couldn't have been found"<<std::endl;
	terminateAction = true;
}

int main(int argc,char* argv[]){
	
	std::string nodeName = "Strip_Follower";
	ros::init(argc,argv,nodeName.c_str());
	ros::NodeHandle nh;
	
	pauseNode = false;
	terminateAction = true;
	actionflag = false;
	
	previousError = 0;
	totalError = 0;
	desired_speed = 1.0;
	leftMotorID = 0;
	rightMotorID = 0;

	//publishers
	pubMotorSpeeds			= nh.advertise<vrep_common::JointSetStateData>("/Motors",1);
	ros::Publisher pubCompletionInfo 		= nh.advertise<std_msgs::Bool>("/controls/completed",1,true);
	
	//subscriber
	ros::Subscriber subHardwareInfo 		= nh.subscribe("/sensorControl/hardwareOk",1,hardwareCallBack);
	ros::Subscriber subLocation 			= nh.subscribe("/vrep/RobotLocation",1,locationCallBack);
	ros::Subscriber subCommands 			= nh.subscribe("/stripFollowDirectives",1,directivesCallBack);
	ros::Subscriber subLeftInductiveSensor 	= nh.subscribe("/vrep/leftInductiveSensorDistance",1,leftInductiveSensorCallBack);
	ros::Subscriber subRightInductiveSensor = nh.subscribe("/vrep/rightInductiveSensorDistance",1,rightInductiveSensorCallBack);
	ros::Subscriber subPause 				= nh.subscribe("/controls/pause",1,pauseCallBack);
	ros::Subscriber subResume 				= nh.subscribe("/controls/resume",1,resumeCallBack);
	ros::Subscriber subTerminate 			= nh.subscribe("/controls/terminate",1,terminateCallBack);
	ros::Subscriber subleftMotorID			= nh.subscribe("/motorID/left",1,leftMotorIDCallBack);
	ros::Subscriber subrightMotorID 		= nh.subscribe("/motorID/right",1,rightMotorIDCallBack);
	
	ros::spinOnce();
	
	///////////////
	
	while(ros::ok()){	//main cycle
		
		vrep_common::JointSetStateData motorSpeeds;
		
		ros::spinOnce();
		
		// if Inductive sensor datas doesn't change --- robot lost the strip
		if(actionflag && !terminateAction && stripStatus()){
			std::cout<<"Strip Follower  --- Lost the strip"<<std::endl;
			searchStrip();
		}
		
		if(travel_distance > getDistance() && actionflag){//do the work
			
			if(terminateAction || pauseNode){
				vrep_common::JointSetStateData motorStop;
			
				motorStop.handles.data.push_back(leftMotorID);
				motorStop.setModes.data.push_back(2);
				motorStop.values.data.push_back(0);
				
				motorStop.handles.data.push_back(rightMotorID);
				motorStop.setModes.data.push_back(2);
				motorStop.values.data.push_back(0);
				
				pubMotorSpeeds.publish(motorStop); //stop robot
			}
			else{
				motorSpeedCalculator(motorSpeeds);
				pubMotorSpeeds.publish(motorSpeeds); //send motor speeds to follow
			}
		}
		
		else if(actionflag){//completed
			vrep_common::JointSetStateData motorStop;
			
			motorStop.handles.data.push_back(leftMotorID);
			motorStop.setModes.data.push_back(2);
			motorStop.values.data.push_back(0);
			
			motorStop.handles.data.push_back(rightMotorID);
			motorStop.setModes.data.push_back(2);
			motorStop.values.data.push_back(0);
			
			pubMotorSpeeds.publish(motorStop);  //stop robot
			
			if(terminateAction){ //if termination arrives
				std::cout<<"Strip Follower  --- Terminated"<<std::endl;
			}
			else{
				std::cout<<"Strip Follower  --- Completed"<<std::endl;
				
				std_msgs::Bool completed;
				completed.data = true;
				pubCompletionInfo.publish(completed);  //publish completed info
			}
			actionflag = false;
		}
		
		while(pauseNode){ //if pause arrives
			ros::spinOnce();
		}
	
	}
} 
