//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <ros/ros.h>
#include <iostream>
#include <cstdio>

#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "sensor_msgs/JointState.h"

// Used data structures:
#include "vrep_common/JointSetStateData.h"
#include "vrep_common/VrepInfo.h"
#include "wall_follower/Status.h"



#define THROTTLE_SPEED 1.9f
#define PROPORTIONAL_CONSTANT 0.57f
#define DERIVATIVE_CONSTANT 0.052f
#define INTEGRAL_CONSTANT 0.000001f

#define PI 3.14159265

bool completeMessage = false ;
// if it is zero, left sensor data, if it is one, right sensor data

//direction variable for which wall to follow, 0 is left 1 is right 
int direction = 0 ;
float directionAngle = 0.0 ;
// target following distance for the robot
float followingDistance = 0 ;
//total travel distance of the robot
float travelDistanceLength = 0 ;

int detectedObject = 0 ;
float relativeDistance = 0;

//PID control related variables
float previousError = 0.0f;
float totalError = 0.0f ;

//Total travelling distance of the robot
float travelledDistance = 0.0f ;

bool ordersTaken = false ;

float detectedDistance[2] = {};

//Required for Robot Location and rotation calculation 
//initial alpha rotation value
float initialRot = 0.0;
float coverInitialRot = 0.0;
// check variable for initial rotation
bool initialRotations = false ;
bool coverInitialRotations = false ;
//check variable for initial locations
bool initialLocations = true ;
//First location values of the robot 
float locationF[3] = {} ; 
float locations[3] = {};
float coverRotations[3] = {};
float rotations[3] = {};
float coverQ0,coverQ1,coverQ2,coverQ3 = 0.0;
float q0,q1,q2,q3 = 0.0;
float sensorToWall[2]={};

bool pauseMotion = false; 

bool simulationGO = false ;

bool isHardwareOk = true ;

bool followCompletion = false ;

float coverPosition = 0.0 ;

//Motor id holders
float leftM,rightM,coverM,coverAngle,skirtM = 0.0 ;

//manouevre Variables
float errorHandle ;
float errorC_P ;
float manouevreValue ;

//Callback functions

void sensorCallBack(const boost::shared_ptr<std_msgs::Float32>& msg){
	relativeDistance = msg->data ;
//	std::cout << "Relative distance =  " << relativeDistance << std::endl;
}

void leftDistanceCallBack(const boost::shared_ptr<std_msgs::Float32>& distance){
	sensorToWall[0] = distance->data ;
	detectedDistance[0]   = (sensorToWall[0] * cos((rotations[0] - initialRot) * PI / 180.0));
	//std::cout <<"Left detected =  "<< detectedDistance[0]  << std::endl ;
}

void rightDistanceCallBack(const boost::shared_ptr<std_msgs::Float32>& distance){
	sensorToWall[1] = distance->data ;
	detectedDistance[1]  = (sensorToWall[1] * cos((rotations[0] - initialRot) * PI / 180.0));
	//std::cout <<"Right detected =  "<< detectedDistance[1]  << std::endl ;
}


void locationCallBack(const boost::shared_ptr<geometry_msgs::PoseStamped>& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		initialRotations = true ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;

        completeMessage = false ;
        simulationGO = true ;
        initialLocations = true; 
	}
	//std::cout << locations[0] << "||" << locations[1] << "||" << locations[2] << std::endl;
	travelledDistance = std::sqrt(std::pow((locations[0] - locationF[0]),2) + std::pow((locations[1] - locationF[1]),2)+std::pow((locations[2] - locationF[2]),2) );
}

void coverAngleCallBack(const boost::shared_ptr<sensor_msgs::JointState>& location){

    coverPosition = location->position[0] ;
}

void directiveCallBack(const boost::shared_ptr<std_msgs::Float32MultiArray>& directives){
	
	direction = directives->data[0];
	followingDistance = directives->data[1];
	travelDistanceLength = directives->data[2];
    coverInitialRotations = false;
	ordersTaken = true ;
    initialLocations =false;
}

void pauseCallBack(const boost::shared_ptr<std_msgs::Empty>& info){
	pauseMotion = true ;
}
void resumeCallBack(const boost::shared_ptr<std_msgs::Empty>& info){
	pauseMotion = false ;
}
void terminateCallBack(const boost::shared_ptr<std_msgs::Empty>& info){
	ordersTaken = false ;
	simulationGO = false ;
}

std_msgs::Float64MultiArray manoeuvre(float currentDistance){
	//Create a default message 
	std_msgs::Float64MultiArray msg ;
	//error for the current distance to the obstacle
	errorHandle = currentDistance - followingDistance ;
	totalError += errorHandle ;
	errorC_P = errorHandle - previousError ;

	 manouevreValue = (PROPORTIONAL_CONSTANT * errorHandle) +
							 (DERIVATIVE_CONSTANT * errorC_P) + 
							 (INTEGRAL_CONSTANT * totalError) ;

	previousError = errorHandle ;

	float leftSpeed = 0.0;
	float rightSpeed =0.0;
	

	if(currentDistance > followingDistance){
		leftSpeed = THROTTLE_SPEED + (manouevreValue * (direction));
		rightSpeed = THROTTLE_SPEED + (manouevreValue * (1 - direction) ) ;
		
	}else{	
		leftSpeed =THROTTLE_SPEED - (manouevreValue * (1 - direction));
		rightSpeed=THROTTLE_SPEED - (manouevreValue * (direction )) ;
		
	}
	if(  (rotations[0] - initialRot) < -15 && (leftSpeed > rightSpeed) ){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;
		
	}else if( (rotations[0] - initialRot) > 15 && ( rightSpeed > leftSpeed )){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;	
			
		}
	if(travelDistanceLength < travelledDistance ) {
		leftSpeed = 0.0 ;
		rightSpeed = 0.0;
		completeMessage = true ;
		simulationGO = false;
	}

	msg.data.push_back(leftSpeed);
	msg.data.push_back(rightSpeed);
	return msg ;			
}

std_msgs::Float64MultiArray coverManoeuvre(float direction){
	std_msgs::Float64MultiArray coverMsg ;
	if(direction){
		directionAngle = (130) * PI / 180;
	}else{
		directionAngle = (180) * PI / 180;
	}
	coverMsg.data.push_back(directionAngle);
	return coverMsg ;
}

bool nodeStatusCallBack(wall_follower::Status::Request& req,wall_follower::Status::Response& res){
	res.status = simulationGO ;
	ROS_INFO("Sending status of the Node");
	return true ;
}

int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("wallFollower");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("WallNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS----------------------------------------------------------------------------

	//Default info callback 

	ros::Subscriber leftDistInfo = node.subscribe("/vrep/leftSensorDistance",1,leftDistanceCallBack);

	ros::Subscriber rightDistInfo = node.subscribe("/vrep/rightSensorDistance",1,rightDistanceCallBack);
	
	ros::Subscriber locSub = node.subscribe("/vrep/RobotLocation",1,locationCallBack);

    ros::Subscriber coverAngleInfo = node.subscribe("/vrep/coverAngle",1,coverAngleCallBack);

	// Listens which wall to follow
	ros::Subscriber directiveInfo = node.subscribe("/followDirectives",1,directiveCallBack);


	ros::Subscriber pauseInfo = node.subscribe("/controls/pause",1,pauseCallBack);

	ros::Subscriber resumeInfo = node.subscribe("/controls/resume",1,resumeCallBack);

	ros::Subscriber terminateInfo = node.subscribe("/controls/terminate",1,terminateCallBack);

	ros::ServiceServer nodeService = node.advertiseService("/followerNodeStatus", nodeStatusCallBack);
	//ALL PUBLISHERS----------------------------------------------------------------------------------

	ros::Publisher DirectionInfo = node.advertise<std_msgs::Float32>("/sensorControl/coverDirection",1);

	ros::Publisher completionInfo = node.advertise<std_msgs::Bool>("/controls/completed",1,true);

	ros::Publisher LmotorSpeedPub=node.advertise<std_msgs::Float64>("/Motors/left",1);
	ros::Publisher RmotorSpeedPub=node.advertise<std_msgs::Float64>("/Motors/right",1);
//	ros::Publisher CmotorSpeedPub=node.advertise<std_msgs::Float64>("/Motors/cover",1);
	ros::Publisher CAmotorSpeedPub=node.advertise<std_msgs::Float64>("/Motors/coverAngle",1);

	std_msgs::Float64MultiArray motorSpeeds ;
	std_msgs::Float64 leftMotor;
	std_msgs::Float64 rightMotor ;
	std_msgs::Float64MultiArray coverSpeed ;
	std_msgs::Float64 coverM;
	std_msgs::Float64 coverAngleM ;

	//Simulation main loop
	while(ros::ok() ){

	if(!pauseMotion){

		if(ordersTaken){
			coverSpeed = coverManoeuvre(direction);
			//coverM.data = coverSpeed.data[0];
			coverAngleM.data = coverSpeed.data[0];
			//CmotorSpeedPub.publish(coverM);
			CAmotorSpeedPub.publish(coverAngleM);
			ordersTaken = false; 
		}
      

	if(simulationGO && 
		(coverPosition < directionAngle + 0.01 && coverPosition > directionAngle - 0.01)){	
		
		motorSpeeds = manoeuvre(detectedDistance[direction]);
		leftMotor.data = motorSpeeds.data[0];
		rightMotor.data = motorSpeeds.data[1];
		LmotorSpeedPub.publish(leftMotor);
		RmotorSpeedPub.publish(rightMotor);
		std::cout << "follow task in progress"<< std::endl ;
	}
			//TODO 
			//very bad coding ?
		
	if(completeMessage){
		std_msgs::Bool msg ;
		msg.data = true;
		completionInfo.publish(msg);
		std::cout << "follow task completed" << std::endl ;
		completeMessage = false;
	}
		
	}
	
		ros::spinOnce() ;

		usleep(1000);
	}

	ros::shutdown();
	return 0 ;
}
