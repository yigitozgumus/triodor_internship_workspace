# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "turn: 0 messages, 1 services")

set(MSG_I_FLAGS "-Istd_msgs:/opt/ros/jade/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(turn_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/yigit/catkin_ws/src/turn/srv/Status.srv" NAME_WE)
add_custom_target(_turn_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "turn" "/home/yigit/catkin_ws/src/turn/srv/Status.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(turn
  "/home/yigit/catkin_ws/src/turn/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/turn
)

### Generating Module File
_generate_module_cpp(turn
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/turn
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(turn_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(turn_generate_messages turn_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/turn/srv/Status.srv" NAME_WE)
add_dependencies(turn_generate_messages_cpp _turn_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(turn_gencpp)
add_dependencies(turn_gencpp turn_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS turn_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services
_generate_srv_eus(turn
  "/home/yigit/catkin_ws/src/turn/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/turn
)

### Generating Module File
_generate_module_eus(turn
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/turn
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(turn_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(turn_generate_messages turn_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/turn/srv/Status.srv" NAME_WE)
add_dependencies(turn_generate_messages_eus _turn_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(turn_geneus)
add_dependencies(turn_geneus turn_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS turn_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services
_generate_srv_lisp(turn
  "/home/yigit/catkin_ws/src/turn/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/turn
)

### Generating Module File
_generate_module_lisp(turn
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/turn
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(turn_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(turn_generate_messages turn_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/turn/srv/Status.srv" NAME_WE)
add_dependencies(turn_generate_messages_lisp _turn_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(turn_genlisp)
add_dependencies(turn_genlisp turn_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS turn_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(turn
  "/home/yigit/catkin_ws/src/turn/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/turn
)

### Generating Module File
_generate_module_py(turn
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/turn
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(turn_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(turn_generate_messages turn_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/turn/srv/Status.srv" NAME_WE)
add_dependencies(turn_generate_messages_py _turn_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(turn_genpy)
add_dependencies(turn_genpy turn_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS turn_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/turn)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/turn
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(turn_generate_messages_cpp std_msgs_generate_messages_cpp)

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/turn)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/turn
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
add_dependencies(turn_generate_messages_eus std_msgs_generate_messages_eus)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/turn)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/turn
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(turn_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/turn)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/turn\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/turn
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(turn_generate_messages_py std_msgs_generate_messages_py)
