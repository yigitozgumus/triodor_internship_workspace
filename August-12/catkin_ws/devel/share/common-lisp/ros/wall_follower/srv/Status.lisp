; Auto-generated. Do not edit!


(cl:in-package wall_follower-srv)


;//! \htmlinclude Status-request.msg.html

(cl:defclass <Status-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass Status-request (<Status-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Status-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Status-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name wall_follower-srv:<Status-request> is deprecated: use wall_follower-srv:Status-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Status-request>) ostream)
  "Serializes a message object of type '<Status-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Status-request>) istream)
  "Deserializes a message object of type '<Status-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Status-request>)))
  "Returns string type for a service object of type '<Status-request>"
  "wall_follower/StatusRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Status-request)))
  "Returns string type for a service object of type 'Status-request"
  "wall_follower/StatusRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Status-request>)))
  "Returns md5sum for a message object of type '<Status-request>"
  "3a1255d4d998bd4d6585c64639b5ee9a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Status-request)))
  "Returns md5sum for a message object of type 'Status-request"
  "3a1255d4d998bd4d6585c64639b5ee9a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Status-request>)))
  "Returns full string definition for message of type '<Status-request>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Status-request)))
  "Returns full string definition for message of type 'Status-request"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Status-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Status-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Status-request
))
;//! \htmlinclude Status-response.msg.html

(cl:defclass <Status-response> (roslisp-msg-protocol:ros-message)
  ((status
    :reader status
    :initarg :status
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Status-response (<Status-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Status-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Status-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name wall_follower-srv:<Status-response> is deprecated: use wall_follower-srv:Status-response instead.")))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <Status-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wall_follower-srv:status-val is deprecated.  Use wall_follower-srv:status instead.")
  (status m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Status-response>) ostream)
  "Serializes a message object of type '<Status-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'status) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Status-response>) istream)
  "Deserializes a message object of type '<Status-response>"
    (cl:setf (cl:slot-value msg 'status) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Status-response>)))
  "Returns string type for a service object of type '<Status-response>"
  "wall_follower/StatusResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Status-response)))
  "Returns string type for a service object of type 'Status-response"
  "wall_follower/StatusResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Status-response>)))
  "Returns md5sum for a message object of type '<Status-response>"
  "3a1255d4d998bd4d6585c64639b5ee9a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Status-response)))
  "Returns md5sum for a message object of type 'Status-response"
  "3a1255d4d998bd4d6585c64639b5ee9a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Status-response>)))
  "Returns full string definition for message of type '<Status-response>"
  (cl:format cl:nil "bool status~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Status-response)))
  "Returns full string definition for message of type 'Status-response"
  (cl:format cl:nil "bool status~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Status-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Status-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Status-response
    (cl:cons ':status (status msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Status)))
  'Status-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Status)))
  'Status-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Status)))
  "Returns string type for a service object of type '<Status>"
  "wall_follower/Status")