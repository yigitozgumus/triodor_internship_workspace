//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"


// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
//#include "vrep_common/JointSetStateData.h"
#include "vrep_common/simRosEnablePublisher.h"

#define MANOUEVRE_LIMIT 2.0f
#define THROTTLE_SPEED 3.0f 
#define PROPORTIONAL_CONSTANT 0.50f
#define DERIVATIVE_CONSTANT 0.08f
#define INTEGRAL_CONSTANT 0.0000005f

#define PI 3.14159265


//Global variables

int detectedObj = 0 ;
int detectedObject = 0 ;
float relativeDistance = 0;
bool simulationRunning = true ;
float initialRot = 0.0;
bool initialPosition = false ;
float previousError = 0.0f;
float totalError = 0.0f ;


float initialX, initialY , initialZ = 0.0 ;
float xRot, yRot,zRot  = 0.0 ;
float q0,q1,q2,q3 = 0.0;
float x,y,z = 0.0 ; // Sensor data message
//Twist message arrays


float detectedDistance = 0;
int objectDistanceHandle = 0;

float sensorToWall = 0;
//Callback functions
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info)
{
	simulationRunning=(info->simulatorState.data&1)!=0;
}

void sensorCallBack(const std_msgs::Float32::ConstPtr& msg){
	relativeDistance = msg->data ;
	std::cout << "Relative distance =  " << relativeDistance << std::endl;


}

void distanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){
	objectDistanceHandle = info->detectedObject.data ;
	x = info->detectedPoint.x ;
	y = info->detectedPoint.y ;
	z = info->detectedPoint.z ;

	sensorToWall = std::sqrt(std::pow(x,2) + std::pow(y,2) + std::pow(z,2));
	//detectedDistance = (sensorToWall * cos((initialRot - xLoc) * PI / 180.0));
	detectedDistance = sensorToWall;
	std::cout <<"detected distance is: "<< detectedDistance  << std::endl ;

}

void locationCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	xRot = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	yRot = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	zRot = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	if(!initialPosition){
		initialRot = xRot;
		initialPosition = true ;
	}
	
}



std_msgs::Float32MultiArray manoeuvre(float currentDistance){
	//Create a default message 
	std_msgs::Float32MultiArray msg ;
	//error for the current distance to the obstacle
	float errorHandle = currentDistance - MANOUEVRE_LIMIT ;
	totalError += errorHandle ;
	float errorC_P = errorHandle - previousError ;

	float manouevreValue = (PROPORTIONAL_CONSTANT * errorHandle) +
							 (DERIVATIVE_CONSTANT * errorC_P) + 
							 (INTEGRAL_CONSTANT * totalError) ;
	previousError = errorHandle ;


	if(currentDistance > MANOUEVRE_LIMIT){
		if( ((initialRot- xRot) < 15 && (initialRot - xRot) > -15)  ){
			msg.data.push_back(THROTTLE_SPEED );
			msg.data.push_back(THROTTLE_SPEED + manouevreValue);
			return msg ;
		}else{
			msg.data.push_back(THROTTLE_SPEED);
			msg.data.push_back(THROTTLE_SPEED);
			return msg ;
		}
	}else{	
		msg.data.push_back(THROTTLE_SPEED - manouevreValue);
		msg.data.push_back(THROTTLE_SPEED );
		return msg ;
	}

	
}

int main(int argc,char* argv[]){

	int _argc = 0;
	char** _argv = NULL;
	struct timeval tv;
	unsigned int timeVal=0;
	if (gettimeofday(&tv,NULL)==0)
		timeVal=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
	std::string nodeName("wallFollower");
	// std::string randId(boost::lexical_cast<std::string>(timeVal+int(999999.0f*(rand()/(float)RAND_MAX))));
	// nodeName+=randId;
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("WallNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	ros::Subscriber subInfo=node.subscribe("/vrep/info",1,infoCallback);

	ros::Subscriber sub=node.subscribe("/vrep/ProximitySensorData",1,distanceCallBack);

	 ros::Subscriber locSub = node.subscribe("/vrep/RobotLocation",1,locationCallBack);


	ros::Publisher VelocityPub = node.advertise<std_msgs::Float32MultiArray>("velocity",1);

	while(ros::ok && simulationRunning){

		std_msgs::Float32MultiArray msg ;
		msg = manoeuvre(detectedDistance);
		VelocityPub.publish(msg);


		ros::spinOnce() ;

		usleep(1000);
	}

	return 0 ;
}
