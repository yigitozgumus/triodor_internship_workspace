//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>

#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"

#define PI 3.14159265

int direction = 0 ; // -1 is for backwards , 1 is for forwards
float speed = 0.0f ;


bool isHardwareOk = false ;

float locations[3] = {};
float rotations[3] = {};
float q0,q1,q2,q3 = 0.0;
//First location values of the robot 
float locationF[3] = {} ;
// check variable for initial rotation
bool initialRotations = false ; 
bool initialLocations = false ;
//initial alpha rotation value
float initialRot = 0.0;
float travelledDistance = 0.0f ;
float travelDistanceLength = 0.0f ;

bool ordersTaken = false ;
bool simulationGOm = false ; // m for motion
bool motionCompletion = false ;

//Callback functions
void hardwareCallBack(const std_msgs::Bool::ConstPtr& check){
	isHardwareOk = check->data;
}

void locCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	//std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		initialRotations = true ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;
		initialLocations = true; 
		motionCompletion = false;
	}

	travelledDistance = std::sqrt(std::pow((locations[0] - locationF[0]),2) + std::pow((locations[1] - locationF[1]),2) );
}

void motionInfoCallBack(const std_msgs::Float32MultiArray::ConstPtr& info){
	if(travelDistanceLength < travelledDistance){
		ordersTaken = false;
		initialLocations = false ;
	}
	if(!ordersTaken){
	direction = info->data[0];
	speed = info->data[1];
	travelDistanceLength = info->data[2];
	ordersTaken = true ;
	}
}

std_msgs::Float32MultiArray startMotion(){
	std_msgs::Float32MultiArray msg; 
	float leftSpeed = speed * direction ;
	float rightSpeed = speed * direction ;
	
	if(travelDistanceLength < travelledDistance) {
		leftSpeed = 0.0 ;
		rightSpeed = 0.0;
		motionCompletion = true ;
	} 

	msg.data.push_back(leftSpeed);
	msg.data.push_back(rightSpeed);
	return msg ;

}


int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("straightMotion");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("straightNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS------------------------------------------------------

	ros::Subscriber hardwareInfo = node.subscribe("/sensorControl/hardwareOk",1,hardwareCallBack);

	ros::Subscriber localSub = node.subscribe("/vrep/RobotLocation",1,locCallBack);

	ros::Subscriber straightSub = node.subscribe("/straightDirective",1,motionInfoCallBack);


	//ALL PUBLISHERS ------------------------------------------------------

	ros::Publisher leftVelocityP = node.advertise<std_msgs::Float32>("/sensorControl/leftVelocity",1);

	ros::Publisher rightVelocityP = node.advertise<std_msgs::Float32>("/sensorControl/rightVelocity",1);

	ros::Publisher completionInfo = node.advertise<std_msgs::String>("CompletedInfo",1);


	//Simulation main loop
	while(ros::ok){
		if(isHardwareOk){

			if(ordersTaken){
				simulationGOm = !motionCompletion ;
			}

			if(simulationGOm){
				std_msgs::Float32 left ;
				std_msgs::Float32 right ;
				std_msgs::Float32MultiArray msg ;
			
				msg = startMotion();

				left.data = msg.data[0];
				right.data = msg.data[1];

				leftVelocityP.publish(left);
				rightVelocityP.publish(right);
				std::cout << "straight motion in progress"<< std::endl ;
			
			}
			if(motionCompletion){
				std_msgs::String msg ;
				msg.data = "I completed the task!\n";
				completionInfo.publish(msg);
				std::cout << "straight motion completed" << std::endl ;
			}

		}
		// handle ROS messages:
		ros::spinOnce();

		// sleep a bit:
		usleep(1000);

	}
	
	return 0 ;
}