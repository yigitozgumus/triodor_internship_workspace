//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"

// Used data structures:
#include "vrep_common/ProximitySensorData.h"
#include "vrep_common/VrepInfo.h"
//#include "vrep_common/JointSetStateData.h"
#include "vrep_common/simRosEnablePublisher.h"

#define THROTTLE_SPEED 1.8f 
#define PROPORTIONAL_CONSTANT 0.55f
#define DERIVATIVE_CONSTANT 0.08f
#define INTEGRAL_CONSTANT 0.000005f

#define PI 3.14159265

// if it is zero, left sensor data, if it is one, right sensor data


//direction variable for which wall to follow, 0 is left 1 is right 
int direction = 0 ;
// target following distance for the robot
float followingDistance = 2.0 ;
//total travel distance of the robot
float travelDistanceLength = 5 ;
//initial alpha rotation value
float initialRot = 0.0;
// check variable for initial rotation
bool initialRotations = false ;
//First location values of the robot 
float locationF[3] = {} ; 
//check variable for initial locations
bool initialLocations = false ;

int detectedObject = 0 ;
float relativeDistance = 0;
bool simulationRunning = true ;

//PID control related variables
float previousError = 0.0f;
float totalError = 0.0f ;

//Total travelling distance of the robot
float travelledDistance = 0.0f ;

bool ordersTaken = false ;


//Required for Robot Location and rotation calculation 
float locations[3] = {};
float rotations[3] = {};
float q0,q1,q2,q3 = 0.0;
float x[2],y[2],z[2] = {} ; // Sensor data message



float detectedDistance[2] = {};
float sensorToWall[2]={};


bool simulationGOGO = false ;

//Callback functions
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info)
{
	simulationRunning=(info->simulatorState.data&1)!=0;
}

void sensorCallBack(const std_msgs::Float32::ConstPtr& msg){
	relativeDistance = msg->data ;
	std::cout << "Relative distance =  " << relativeDistance << std::endl;


}

void leftDistanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){
	
	x[0] = info->detectedPoint.x ;
	y[0] = info->detectedPoint.y ;
	z[0] = info->detectedPoint.z ;

	sensorToWall[0] = std::sqrt(std::pow(x[0],2) + std::pow(y[0],2) + std::pow(z[0],2));
	detectedDistance[0] = (sensorToWall[0] * cos((rotations[0] - initialRot) * PI / 180.0));
	//detectedDistance[0] = sensorToWall[0];
	std::cout <<"Left detected =  "<< detectedDistance[0]  << std::endl ;

}

void rightDistanceCallBack(const vrep_common::ProximitySensorData::ConstPtr& info){
	
	x[1] = info->detectedPoint.x ;
	y[1] = info->detectedPoint.y ;
	z[1] = info->detectedPoint.z ;

	sensorToWall[1] = std::sqrt(std::pow(x[1],2) + std::pow(y[1],2) + std::pow(z[1],2));
	detectedDistance[1] = (sensorToWall[1] * cos((rotations[0] - initialRot) * PI / 180.0));
	//detectedDistance[1] = sensorToWall[1];
	std::cout <<" Right detected =  "<< detectedDistance[1] << std::endl;
}
void locationCallBack(const geometry_msgs::PoseStamped::ConstPtr& location){

	q0 =location->pose.orientation.x; 		
	q1 =location->pose.orientation.y;
	q2 =location->pose.orientation.z;
	q3=location->pose.orientation.w;
	rotations[0] = atan2(2 * ((q0 * q1) + (q2*q3)),1- 2*(std::pow(q1,2) + std::pow(q2,2))) * 180.0 / PI;
	rotations[1] = asin(2 * ((q0 * q2) - (q3*q1))) * 180.0 / PI;
	rotations[2] = atan2(2 * ((q0 * q3) + (q1*q2)),1- 2*(std::pow(q2,2) + std::pow(q3,2))) * 180.0 / PI ;
	std::cout << rotations[0] - initialRot  << std::endl ;
	if(!initialRotations){
		initialRot = rotations[0];
		initialRotations = true ;
	}
	locations[0] = location->pose.position.x ;
	locations[1] = location->pose.position.y ;
	locations[2] = location->pose.position.z ;
	if(!initialLocations){
		locationF[0] = location->pose.position.x ;
		locationF[1] = location->pose.position.y ;
		locationF[2] = location->pose.position.z ;
		initialLocations = true; 
	}

	travelledDistance = std::sqrt(std::pow((locations[0] - locationF[0]),2) + std::pow((locations[1] - locationF[1]),2) );
}
void permissionCallBack(const std_msgs::Bool::ConstPtr& gogo){
	if(gogo->data){
		simulationGOGO = true ;
	}
}

void directiveCallBack(const std_msgs::Float32MultiArray::ConstPtr& directives){
	if(travelDistanceLength < travelledDistance){
		ordersTaken = false;
		initialLocations = false ;
	}
	if(!ordersTaken){
		direction = directives->data[0];
		followingDistance = directives->data[1];
		travelDistanceLength = directives->data[2];
		ordersTaken = true ;
	}
}



std_msgs::Float32MultiArray manoeuvre(float currentDistance){
	//Create a default message 
	std_msgs::Float32MultiArray msg ;
	//error for the current distance to the obstacle
	float errorHandle = currentDistance - followingDistance ;
	totalError += errorHandle ;
	float errorC_P = errorHandle - previousError ;

	float manouevreValue = (PROPORTIONAL_CONSTANT * errorHandle) +
							 (DERIVATIVE_CONSTANT * errorC_P) + 
							 (INTEGRAL_CONSTANT * totalError) ;

	previousError = errorHandle ;

	float leftSpeed = 0;
	float rightSpeed =0;

	if(currentDistance > followingDistance){
		leftSpeed = THROTTLE_SPEED + (manouevreValue * (direction));
		rightSpeed = THROTTLE_SPEED + (manouevreValue * (1 - direction) ) ;
		
	}else{	
		leftSpeed =THROTTLE_SPEED - (manouevreValue * (1 - direction));
		rightSpeed=THROTTLE_SPEED - (manouevreValue * (direction )) ;
		
	}
	if(  (rotations[0] - initialRot) < -15 && (leftSpeed > rightSpeed) ){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;
		
	}else if( (rotations[0] - initialRot) > 15 && ( rightSpeed > leftSpeed )){
			leftSpeed=THROTTLE_SPEED;
			rightSpeed=THROTTLE_SPEED;	
			
		}

	if(travelDistanceLength < travelledDistance) {
		leftSpeed = 0.0 ;
		rightSpeed = 0.0;
	}

	msg.data.push_back(leftSpeed);
	msg.data.push_back(rightSpeed);
	return msg ;
		
	
}

int main(int argc,char* argv[]){

	//Node Initialization
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("wallFollower");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("WallNode");
	printf("Wall just started with node name %s\n",nodeName.c_str());

	//ALL SUBSCRIBERS----------------------------------------------------------------------------

	//Default info callback 
	ros::Subscriber subInfo=node.subscribe("/vrep/info",1,infoCallback);

	ros::Subscriber subLeft =node.subscribe("/vrep/LeftProximitySensorData",1,leftDistanceCallBack);

	ros::Subscriber subRight =node.subscribe("/vrep/RightProximitySensorData",1,rightDistanceCallBack);
	
	ros::Subscriber locSub = node.subscribe("/vrep/RobotLocation",1,locationCallBack);

	// Listens Ok message from wallFollower node 
	ros::Subscriber permissionSub = node.subscribe("/sensorControl/Permission",1,permissionCallBack); 

	// Listens which wall to follow
	ros::Subscriber directiveInfo = node.subscribe("/vrep/Directives",1,directiveCallBack);

	//ALL PUBLISHERS----------------------------------------------------------------------------------

	ros::Publisher VelocityPub = node.advertise<std_msgs::Float32MultiArray>("velocity",1);

	ros::Publisher DirectionInfo = node.advertise<std_msgs::Int32>("whichWall",1);

	//Simulation main loop
	while(ros::ok && simulationRunning){

		if(ordersTaken){
		std_msgs::Int32 initial ;
		initial.data = direction ;
		DirectionInfo.publish(initial);
	}

		if(simulationGOGO ){
		std_msgs::Float32MultiArray msg ;
		msg = manoeuvre(detectedDistance[direction]);
		VelocityPub.publish(msg);
	}

		ros::spinOnce() ;

		usleep(1000);
	}

	return 0 ;
}
